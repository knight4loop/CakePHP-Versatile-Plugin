<?php
namespace Versatile\Controller\Component;

use Cake\Controller\Component\AuthComponent as CakeAuthComponent;
use Versatile\Controller\Component\GlobalComponentTrait;

class AuthComponent extends CakeAuthComponent
{
    use GlobalComponentTrait;
}
