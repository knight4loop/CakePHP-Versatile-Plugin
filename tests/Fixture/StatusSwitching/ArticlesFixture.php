<?php
namespace Versatile\Test\Fixture\StatusSwitching;

use Cake\TestSuite\Fixture\TestFixture;

class ArticlesFixture extends TestFixture {
    /**
     * fields property
     *
     * @var array
     */
    public $fields = [
        'id' => ['type' => 'integer', 'key' => 'primary'],
        'user_id' => ['type' => 'integer', 'null' => true],
        'title' => ['type' => 'string', 'null' => true],
        'body' => 'text',
        'published' => ['type' => 'string', 'length' => 1, 'default' => 'N'],
        'is_disabled' => ['type' => 'integer', 'null' => true, 'default' => 0],
        'disabled' => ['type'=>'datetime', 'null' => true],
        'created' => ['type' => 'datetime', 'null' => false],
        'updated' => ['type' => 'datetime', 'null' => false],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id']]
        ]
    ];

    /**
     * records property
     *
     * @var array
     */
    public $records = [
        ['user_id' => 1, 'title' => 'First Article', 'body' => 'First Article Body', 'is_disabled' => 0, 'disabled' => null, 'published' => 'Y', 'created' => '2007-03-18 10:39:23', 'updated' => '2007-03-18 10:41:31'],
        ['user_id' => 3, 'title' => 'Second Article', 'body' => 'Second Article Body', 'is_disabled' => 0, 'disabled' => null, 'published' => 'Y', 'created' => '2007-03-18 10:41:23', 'updated' => '2007-03-18 10:43:31'],
        ['user_id' => 1, 'title' => 'Third Article', 'body' => 'Third Article Body', 'is_disabled' => 1, 'disabled' => '2009-09-11 23:24:11', 'published' => 'Y', 'created' => '2007-03-18 10:43:23', 'updated' => '2007-03-18 10:45:31']
    ];

}
