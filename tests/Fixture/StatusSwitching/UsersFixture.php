<?php
namespace Versatile\Test\Fixture\StatusSwitching;

use Cake\TestSuite\Fixture\TestFixture;

class UsersFixture extends TestFixture
{
    /**
     * fields property
     *
     * @var array
     */
    public $fields = [
        'id' => ['type' => 'integer', 'key' => 'primary'],
        'user' => ['type' => 'string', 'null' => true],
        'password' => ['type' => 'string', 'null' => true],
        'is_disabled' => ['type' => 'integer', 'null' => true, 'default' => 0],
        'disabled' => ['type'=>'datetime', 'null' => true],
        'created' => ['type' => 'datetime', 'null' => false],
        'updated' => ['type' => 'datetime', 'null' => false],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id']]
        ]
    ];

    /**
     * records property
     *
     * @var array
     */
    public $records = [
        ['user' => 'mariano', 'password' => '5f4dcc3b5aa765d61d8327deb882cf99', 'is_disabled' => 0, 'disabled' => null, 'created' => '2007-03-17 01:16:23', 'updated' => '2007-03-17 01:18:31'],
        ['user' => 'nate', 'password' => '5f4dcc3b5aa765d61d8327deb882cf99', 'is_disabled' => 0, 'disabled' => null, 'created' => '2007-03-17 01:18:23', 'updated' => '2007-03-17 01:20:31'],
        ['user' => 'larry', 'password' => '5f4dcc3b5aa765d61d8327deb882cf99', 'is_disabled' => 1, 'disabled' => '2010-12-30 11:42:30', 'created' => '2007-03-17 01:20:23', 'updated' => '2007-03-17 01:22:31'],
        ['user' => 'garrett', 'password' => '5f4dcc3b5aa765d61d8327deb882cf99', 'is_disabled' => 1, 'disabled' => '2010-12-30 11:42:30', 'created' => '2007-03-17 01:22:23', 'updated' => '2007-03-17 01:24:31'],
    ];
}
