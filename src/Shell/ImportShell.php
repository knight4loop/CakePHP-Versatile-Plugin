<?php
/**
 * ImportShell
 *
 * @package
 * @copyright
 * @author k.naito <k.naito@coosy.co.jp>
 */
namespace Versatile\Shell;

use Cake\Core\Configure;
use Cake\Core\ConventionsTrait;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Utility\Inflector;
use Versatile\Console\VersatileShell;
use Versatile\Datafile\Converter;
use Versatile\Datafile\Datafile;

class ImportShell extends VersatileShell
{

    use ConventionsTrait;

    /**
     * The db connection being used for baking
     *
     * @var string
     */
    public $connection = null;

    /**
     * Datafile
     *
     * @var string
     */
    public $Datafile = null;

    /**
     * Gets the option parser instance and configures it.
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->description(
            "ファイルからデータを読み取ってデータベースに登録（インポート）します。\n" .
            "読み取るファイルやインポート先のテーブルはコマンドラインオプションで指定してください。\n" .
            "デフォルトでは Configure::read('Versatile.ImportShell') の設定値が使用されます。\n" .
            "Usage : \n" .
            "Versatile.import all   ... 全てのテーブルに対応するファイルを読み込みます。\n" .
            "Versatile.import label ... label テーブルに対応するファイルを読み込みます。\n"
        )->addOption('path', [
            'help' => "指定したファイルもしくはディレクトリが使用されます。",
            'default' => Configure::read('Versatile.ImportShell.path'),
            'short' => 'p'
        ])->addOption('engine', [
            'help' => "指定したファイル形式（拡張子）で読み込みます。(yaml/csv/excel)",
            'default' => Configure::read('Versatile.ImportShell.engine'),
            'short' => 'e'
        ])->addOption('not-auto-increment', [
            'short' => 'i',
            'boolean' => true,
            'help' => 'Primary key が設定のされてない Auto increment のカラムに対して 1 からの採番を解除します。'
        ])->addOption('connection', [
            'short' => 'c',
            'default' => 'default',
            'help' => 'インポート先の接続するデータベース設定を指定します。'
        ])->addOption('force', [
            'short' => 'f',
            'help' => '取り込みの際の確認メッセージを表示しません。',
            'boolean' => true,
        ]);

        return $parser;
    }

    public function startup ()
    {
        parent::startup();

        $this->Datafile = new Datafile();
        $this->connection = $this->param('connection');
    }

    /**
     * データインポートを実施します。
     *
     * <p>
     * ※uses に定義があるモデルのみ実施します。
     * </p>
     *
     * @access public
     * @return void
     */
    public function main()
    {
        $path = realpath($this->param('path'));

        if ($this->args) {
            if ($this->args[0] === 'all') {
                // all
                $this->out('可能なかぎり全てのファイルを取り込みます。');
                $tableName = '';
            } else {
                // 引数で指定されたテーブル
                $tableName = $this->_tableName($this->args[0]);
            }

            if (is_file($path)) {
                $files = [$path];
            } else {
                $files = $this->_getAllFiles($tableName);
            }
            if (!empty($files)) {
                $keys = array_values($files);
                $values = array_fill(0, count($keys), $tableName);
                $files = array_combine($keys, $values);
            }
        } else {
            $this->out('取り込みたいテーブルを下記から指定してください。:');
            $this->out('- all');
            $tables = $this->_getEnableTables($path);
            foreach ($tables as $table) {
                $this->out('- ' . $table);
            }
            return true;
        }

        $this->_processMain($files);

        $this->hr();
        $this->out('完了のしました。');
    }

    protected function _processMain ($files)
    {
        $force = $this->param('force');

        $tables = $this->_getAllTables();

        foreach ($files as $filePath => $tableName) {
            if (!is_file($filePath)) {
                continue;
            }

            $Engine = $this->Datafile->getEngine($filePath);
            if (!$Engine) {
                continue;
            }

            $engineName = $this->Datafile->getEngineName($filePath);
            $sheets = $Engine->sheets();
            $filename = basename($filePath);

            foreach ($sheets as $idx => $sheet) {
                $_tableName = '';
                if ($engineName === 'excel') {
                    $_tableName = $this->_tableName($sheet);
                    if (!empty($tableName) && $tableName !== $_tableName) {
                        continue;
                    }
                } else {
                    $_tableName = $tableName;
                    if (empty($_tableName)) {
                        $_tableName = $this->_tableName(pathinfo($sheet)['filename']);
                    }
                }

                if (!in_array($_tableName, $tables)) {
                    $this->out(sprintf('%s はスキップされます。', $_tableName));
                    continue;
                }

                if (!$force) {
                    $select = $this->in(
                        sprintf('%s のテーブルに %s のファイルを取り込みますか？', $_tableName, $filename),
                        ['y', 'n'],
                        'y'
                    );
                    $select = strtolower($select);
                    if (!in_array($select, ['y', 'yes'])) {
                        continue;
                    }
                } else {
                    $this->out(sprintf('%s のテーブルに %s のファイルを取り込みます。', $_tableName, $filename));
                }

                $datas = $Engine->read($sheet);

                if ($_tableName === 'labels') {
                    $datas = Converter::convert($datas, 'FileLabelToArray');
                }

                $this->_processImport($_tableName, $datas);
            }
        }
    }

    /**
     * 指定された配列データにおいて、値が NULL の要素を取り除いた配列を返却します。
     *
     * @param array $data
     * @return array
     */
    protected function _trimNull($data)
    {
        foreach ($data as $key => $v) {
            if (is_null($v)) unset($data[$key]);
        }
        return $data;
    }

    protected function _processImport($modelName, $datas)
    {
        $notAI = $this->param('not-auto-increment');

        $this->loadModel($modelName);
        $Model = $this->{$modelName};

        $db = ConnectionManager::get($this->connection);
        $db->begin();

        try {
            $Model->deleteAll([]);

            $primaryKey = $Model->primaryKey();
            $primaryKeyColumn = $Model->schema()->column($primaryKey);
            $ai = null;
            if (!$notAI && $primaryKeyColumn['autoIncrement']) {
                $ai = 1;
            }

            foreach ($datas as $line) {
                $entity = $Model->newEntity($this->_trimNull($line));
                if ($ai) {
                    if (!$entity->{$primaryKey}) {
                        $entity->{$primaryKey} = $ai;
                    } elseif($ai < $entity->{$Model->primaryKey}) {
                        $ai = $entity->{$Model->primaryKey};
                    } else {
                        throw new ConsoleException("can't save {$Model->table()} :" . var_export($line, true));
                    }
                    $ai++;
                }
                $newEntity = $Model->save($entity);
                if ($newEntity) {
                    $this->out(sprintf("save - %s : %d", $Model->table(), $newEntity->{$primaryKey}));
                } else {
                    throw new ConsoleException("can't save {$Model->table()} :" . var_export($line, true));
                }
            }
            $db->commit();
        } catch (ConsoleException $e) {
            $db->rollback();
            $this->error("do rollback", $e);
        } catch (PDOException $e) {
            $db->rollback();
            $this->error("do rollback", $e);
        }
    }

    protected function _getAllTables ()
    {
        $db = ConnectionManager::get($this->connection);
        if (!method_exists($db, 'schemaCollection')) {
            $this->err(
                'Connections need to implement schemaCollection() to be used with bake.'
            );
            return $this->_stop();
        }
        $schema = $db->schemaCollection();
        $tables = $schema->listTables();
        if (empty($tables)) {
            $this->err('Your database does not have any tables.');
            return $this->_stop();
        }
        sort($tables);
        return $tables;
    }

    protected function _getAllFiles($tableName = null)
    {
        $path = realpath($this->param('path'));
        $engine = $this->param('engine');
        if ($engine === 'all') {
            $files = $this->Datafile->find($path);
            foreach ($files as $idx => $file) {
                if ($this->Datafile->engineName($file) === 'excel') {
                    continue;
                }
                $filename = $this->_tableName(pathinfo($file)['filename']);
                if ($tableName && $filename !== $tableName) {
                    unset($files[$idx]);
                }
            }
        } elseif ($engine === 'excel' || empty($tableName)) {
            $files = $this->Datafile->find($path, [
                'engineName' => $engine,
            ]);
        } elseif ($tableName) {
            $files = $this->Datafile->find($path, [
                'name' => $tableName,
                'engineName' => $engine,
            ]);
        } else {
            $files = [];
        }
        return $files;
    }

    protected function _getEnableTables($path)
    {
        $files = $this->_getAllFiles($path);
        $tables = $this->_getAllTables();
        $enableTable = [];
        foreach ($files as $file) {
            $tableName = $this->_tableName(pathinfo($file)['filename']);
            if (in_array($tableName, $tables)) {
                $enableTable[] = $tableName;
            }
        }
        return array_unique($tables);
    }

    protected function _tableName ($name)
    {
        return Inflector::tableize($name);
    }

}
