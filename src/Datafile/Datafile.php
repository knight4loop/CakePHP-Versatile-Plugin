<?php
namespace Versatile\Datafile;

use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;

class Datafile
{
    /**
     * DatafileEngine と紐付ける拡張子名を指定します。
     * @var
     */
    protected $_engineMap = [
        'yaml' => ['.yaml'],
        'csv' => ['.csv'],
        'excel' => ['.xls', '.xlsx'],
    ];

    /**
     * DatafileEngine のインスタンスが保持されます。
     * @var
     */
    protected $_engines = [];

    /**
     * 引数から使用する DatafileEngine を返します。
     *
     * @param string $path ファイル名もしくは、
     *               $this->_engineMap で指定されている配列キー、
     *               '.'付きの拡張子名で指定します。
     * @return DatafileEngine
     */
    public function getEngine($path)
    {
        $key = $this->getEngineName($path);

        if (!$key) {
            return null;
        }

        if (in_array($key, array_keys($this->_engineMap))) {
            if (!isset($this->_engines[$key])) {
                $className = $this->_engineClassName($key);
                $this->_engines[$key] = new $className();
            }
            $engine = $this->_engines[$key];
        }

        if (is_file(realpath($path)) && $engine) {
            $engine->clear()->path($path);
        }

        return $engine;
    }

    /**
     * 引数から DatafileEngine が使用できるかどうかを返します。
     *
     * @param string $path ファイル名もしくは、
     *               $this->_engineMap で指定されている配列キー、
     *               '.'付きの拡張子名で指定します。
     * @return boolean
     */
    public function isEnablePath($path)
    {
        return !!($this->getEngineName($path));
    }

    /**
     * 引数から使用する DatafileEngine の key を返します。
     *
     * @param string $path ファイル名もしくは、
     *               $this->_engineMap で指定されている配列キー、
     *               '.'付きの拡張子名で指定します。
     * @return string
     */
    public function getEngineName($path)
    {
        if (!$path) {
            return null;
        }

        $ext = $this->_extensionName($path);
        if ($ext) {
            return $this->_getEnginKeyByExt($ext);
        }

        return $this->_getEnginKey($path);
    }

    /**
     * DatafileEngine の key から使用できる拡張子を返します。
     *
     * @param string $key
     * @return array
     */
    public function getExtensions($keys = null)
    {
        if ($keys === null) {
            return $this->_engineMap;
        }

        if (!is_array($keys)) {
            $keys = func_get_args();
        }

        $exts = [];

        foreach ($keys as $key) {
            if (isset($this->_engineMap[$key])) {
                $exts[$key] = $this->_engineMap[$key];
            }
        }

        return $exts;
    }

    /**
     * 指定したパスから使用できるファイルの一覧を返します。
     *
     * @param string $path
     * @param array $settings
     * @return array
     */
    public function find($path, $settings = [])
    {
        $settings += [
            'name' => '.*',
            'engineName' => [],
        ];
        $path = realpath($path);
        $files = [];

        if (empty($settings['engineName'])) {
            $enginesExts = $this->getExtensions();
        } else {
            $enginesExts = $this->getExtensions($settings['engineName']);
        }

        if (empty($enginesExts)) {
            return $files;
        }

        if (is_dir($path)) {
            $folder = new Folder($path);
            $preg = $settings['name'];

            $exts = [];
            foreach ($enginesExts as $_exts) {
                $exts = array_merge($exts, $_exts);
            }
            array_walk($exts, function (&$ext) {
                if (strpos($ext,'.') === 0) {
                    $ext = substr($ext, 1);
                }
                $ext = preg_quote($ext);
            });
            $exts = array_filter($exts);

            if (!empty($exts)) {
                $preg .=  '\.(' . implode('|', $exts) . ')$';
            }

            $files = $folder->find($preg, true);
            foreach ($files as $idx => $file) {
                $files[$idx] = Folder::slashTerm($path) . $file;
            }
        } else {
            $engineName = $this->getEngineName($path);
            if (in_array($engineName, array_keys($enginesExts))) {
                $files = [$path];
            }
        }
        return $files;
    }


    protected function _getEnginKeyByExt($exp)
    {
        $exp = strtolower($exp);
        foreach ($this->_engineMap as $key => $extensions) {
            if (in_array($exp, $extensions)) {
                return $key;
            }
        }
        return null;
    }

    protected function _getEnginKey($key)
    {
        if (in_array($key, array_keys($this->_engineMap))) {
            return $key;
        }
        return null;
    }

    protected function _extensionName($path)
    {
        $ext = null;
        if (strpos($path, '.') === 0) {
            $ext = strtolower($path);
        } elseif (is_file($path)) {
            $pathinfo = pathinfo($path);
            $ext = '.' . $pathinfo['extension'];
        }

        return $ext;
    }

    protected function _engineClassName ($name)
    {
        return '\Versatile\\Datafile\\Engine\\' . Inflector::camelize($name) . 'DatafileEngine';
    }
}
