<?php
namespace Versatile\Test\Fixture\StatusSwitching;

use Cake\TestSuite\Fixture\TestFixture;

class UserDetailsFixture extends TestFixture
{

    /**
     * fields property
     *
     * @var array
     */
    public $fields = [
        'id' => ['type' => 'integer', 'key' => 'primary'],
        'user_id' => ['type' => 'integer', 'null' => false],
        'gender' => ['type' => 'integer', 'null' => true, 'default' => 0],
        'is_disabled' => ['type' => 'integer', 'null' => true, 'default' => 0],
        'disabled' => ['type'=>'datetime', 'null' => true],
        'created' => ['type' => 'datetime', 'null' => false],
        'updated' => ['type' => 'datetime', 'null' => false],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id']]
        ]
    ];

    /**
     * records property
     *
     * @var array
     */
    public $records = [
        ['user_id' => 1, 'gender' => 1, 'is_disabled' => 0, 'created' => '2007-03-17 01:16:23', 'updated' => '2007-03-17 01:18:31'],
        ['user_id' => 2, 'gender' => 1, 'is_disabled' => 0, 'created' => '2007-03-17 01:16:23', 'updated' => '2007-03-17 01:18:31'],
        ['user_id' => 3, 'gender' => 1, 'is_disabled' => 1, 'disabled' => '2010-12-30 11:42:30', 'created' => '2007-03-17 01:16:23', 'updated' => '2007-03-17 01:18:31'],
    ];
}
