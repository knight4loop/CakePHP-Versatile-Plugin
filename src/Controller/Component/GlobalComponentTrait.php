<?php
namespace Versatile\Controller\Component;

use Versatile\Core\GlobalObjectStrage;

trait GlobalComponentTrait
{
    protected $_globalName = null;

    public function initialize(array $config)
    {
        parent::initialize($config);

        if ($this->_globalName) {
            $className = $this->_globalName;
        } else {
            list($namespace, $className) = namespaceSplit(__CLASS__);
            $this->_globalName = $className;
        }

        if (!GlobalObjectStrage::exists($className)) {
            GlobalObjectStrage::set($className, $this);
        }
    }

}
