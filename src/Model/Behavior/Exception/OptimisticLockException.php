<?php
namespace Versatile\Model\Behavior\Exception;

use Cake\Core\Exception\Exception;

class OptimisticLockException extends Exception
{

}
