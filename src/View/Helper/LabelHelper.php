<?php
namespace Versatile\View\Helper;

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\View\Helper;
use Cake\View\View;

class LabelHelper extends Helper
{
    /**
     * @var array
     */
    public $helpers = [
        "Form"
    ];

    /**
     * Default config for this helper.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'select' => [],
        'radio' => [
            "legend" => false
        ],
    ];

    /**
     * @var array
     * @access public
     */
    public $labels;

    /**
     * Constructor hook method.
     *
     * Implement this method to avoid having to overwrite the constructor and call parent.
     *
     * @param array $config The configuration settings provided to this helper.
     * @return void
     */
    public function initialize(array $config)
    {
        if (!Cache::config('versatile_label_helper')) {
            $cacheConfig = [
                'className' => 'File',
                'path' => CACHE,
                'duration' => '+5 minimums',
                'prefix' => 'versatile_label_helper_',
            ];
            if (Configure::read('debug') > 0) {
                $cacheConfig['duration'] = "+1 seconds";
            }
            Cache::config('versatile_label_helper', $cacheConfig);
        }

        $labels = Cache::read('labels');
        if ($labels === false) {
            $flattenLabels = TableRegistry::get('Labels')
                ->find("all", ["order" => ["id ASC"]])
                ->all();
            $labels = [];
            foreach ($flattenLabels as $label) {
                $id = $label->category;
                $labels[$id][] = $label;
            }
            Cache::write('labels', $labels, 'versatile_label_helper');
        }
        $this->labels = $labels;
    }

    /**
     * リスト用の配列を返却します。
     *
     * @param mixed $name
     * @access public
     * @return array
     */
    public function __get($name)
    {
        $get = parent::__get($name);
        if ($get) {
            return $get;
        }
        return $this->selectArray($name);
    }

    /**
     * カテゴリ名とラベルから該当する値を返却します。
     *
     * @param mixed $method
     * @param mixed $args
     * @access public
     * @return void
     */
    public function __call($method, $args)
    {
        return $this->labelByValue($method, $args[0]);
    }

    /**
     * カテゴリ名とラベルから該当する値を返却します。
     *
     * @param mixed $name
     * @param mixed $label
     * @access public
     * @return int
     */
    public function valueByLabel($name, $label)
    {
        $datas = $this->labels[$name];
        foreach ($datas as $data) {
            if ($data->label == $label) {
                return $data->value;
            }
        }
        return false;
    }

    /**
     * カテゴリ名と値から該当するラベルを返却します。
     *
     * @param mixed $name
     * @param mixed $value
     * @access public
     * @return string
     */
    public function labelByValue($name, $value)
    {
        if (!isset($this->labels[$name])) {
            return "";
        }
        $datas = $this->labels[$name];
        foreach ($datas as $data) {
            if ((string)$data->value === (string)$value) {
                return $data->label;
            }
        }
        return "";
    }

    public function labelByListAndValue($list, $value)
    {
        foreach ($list as $val => $name) {
            if ($value == $val) {
                return $name;
            }
        }
        return "";
    }

    /**
     * リスト用の配列を返却します
     *
     * @param mixed $name
     * @access public
     * @return array
     */
    public function selectArray($name, $excludeValues = [])
    {
        !is_array($excludeValues) && $excludeValues = [$excludeValues];

        if (!isset($this->labels[$name])) {
            $this->log(sprintf("%s:%d - label not found : %s", __FILE__, __LINE__, $name));
            $datas = [];
        } else {
            $datas = $this->labels[$name];
        }
        $arr = [];
        foreach ($datas as $data) {
            if (array_search($data->value, $excludeValues) === false) {
                $arr[$data->value] = $data->label;
            }
        }
        return $arr;
    }

    /**
     * comboボックスを作成した結果を返却します。
     *
     * @param mixed $name
     * @param bool $category
     * @param bool $attributes
     * @access public
     * @return string
     */
    public function select($name, $category = null, $attributes = [])
    {
        $cat = $category;
        if (empty($cat)) $cat = $this->_getCategoryFromName($name);
        if (empty($attributes)) $attributes = $this->config('select');
        return $this->Form->select($name, $this->selectArray($cat), $attributes);
    }

    /**
     * radio フォーム HTML を返却します。
     *
     * @param mixed $name
     * @param string $category
     * @param array $attributes
     * @access public
     * @return string
     */
    public function radio($name, $category = null, $attributes = [])
    {
        $cat = $category;
        if (empty($cat)) {
            $cat = $this->_getCategoryFromName($name);
        }
        if (empty($attributes)) {
            $attributes = $this->config('radio');
        }
        return $this->Form->radio($name, $this->selectArray($cat), $attributes);
    }

    /**
     * 指定された値が、マスターに存在するかどうか判定した結果を返却します。
     *
     * @param string $category カテゴリ名
     */
    public function hasValue($category, $value)
    {
        return $this->labelByValue($category, $value) !== "";
    }

    /**
     * フォーム名からカテゴリ名を取り出します。
     * @param string $name
     * @return string
     */
    protected function _getCategoryFromName($name) {
        if (($pos = strpos($name, ".")) !== false) {
            return substr($name, $pos + 1);
        }
        return $name;
    }
}
