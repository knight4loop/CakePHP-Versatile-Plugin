<?php
/**
 * ImportShell
 *
 * @package
 * @copyright
 * @author k.naito <k.naito@coosy.co.jp>
 */
namespace Versatile\Console;

use Cake\Console\Shell;
use Versatile\Core\VersatileInitialize;

class VersatileShell extends Shell
{
    public function initialize() {
        VersatileInitialize::all();
        parent::initialize();
    }
}
