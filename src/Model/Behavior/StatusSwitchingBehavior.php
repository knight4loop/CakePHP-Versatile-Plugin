<?php
/**
 * StatusSwitchingBehavior
 *
 * @author k.naito <k.naito@coosy.co.jp>
 */
namespace Versatile\Model\Behavior;

use ArrayObject;
use Cake\Core\Exception;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\Exception\InvalidPrimaryKeyException;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\ORM\Association;
use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;

class StatusSwitchingBehavior extends Behavior
{

    use TimestampTrait;

    /**
     * Default config
     *
     * Available settings:
     * - field: (string|array, optional) Set to string to use field name of flag.
     *   Set to array to use array key to field name of flag, value to field name of timestamp.
     * - null : (string, optional) Specfiy status enable or disable if value of flag field is null.
     * - status: (bool|int|array, optional) Set to flag status. Valid is 0(false) or 1(true).
     *
     * ```
     * ['status' => 0 ],
     * // enable = 0, disable = 1
     * ```
     *
     * ```
     * ['status' =>  [1, 2]]
     * // enable = 1, disable = 2
     * ```
     *
     * ```
     * ['status' =>  [
     *      'enable' => 1,
     *      'disable' => 2
     * ]]
     * // enable = 1, disable = 2
     * ```
     *
     * @var array $_defaultConfig
     */
    protected $_defaultConfig = array(
        'implementedMethods' => [
            'runTimeStatusSwitching' => 'runTime',
            'disable' => 'disable',
            'enable' => 'enable',
            'disableAll' => 'disableAll',
            'enableAll' => 'enableAll',
        ],
        'field' => 'is_disabled',
        'null' => 'enable',
        'status' => [
            'enable' => 0,
            'disable' => 1,
        ],
    );

    protected $_eventNamePrefix = 'Model';

    /**
     * runTime flag
     *
     * @var bool
     */
    protected $_runTime = true;

    /**
     * Initiate behavior for the model using specified settings.
     *
     * @param Model $model
     * @param array $settings
     * @return void
     */
    public function initialize(array $config)
    {
        $eventNames = [
            'beforeDisable',
            'afterDisable',
            'afterDisableCommit',
            'beforeEnable',
            'afterEnable',
            'afterEnableCommit',
        ];
        $eventManager = $this->_table->eventManager();
        foreach ($eventNames as $eventName) {
            if (method_exists($this->_table, $eventName)) {
                $eventManager->on(
                    $this->_eventNamePrefix . '.' . $eventName,
                    [$this->_table, $eventName]
                );
            }
        }

        $this->runTime(true);

        if (isset($config['status'])) {
            $this->config('status', $config['status'], false);
        }

        parent::initialize($config);
    }

    /**
     * Check behavior is valid, or switch valid/invalid.
     *
     * @param null|bool $isEnable set null to current status. set bool to change current status.
     * @return bool
     */
    public function runTime($isEnable = null)
    {
        if ($isEnable !== null) {
            $this->_runTime = !!$isEnable;
        }
        if (!$this->hasField()) {
            return false;
        }
        return $this->_runTime;
    }

    /**
     * Check table had flag field.
     *
     * @return bool
     */
    public function hasField()
    {
        return $this->_table->hasField($this->config('field'));
    }

    /**
     * beforeFind
     *
     * Add conditions.
     *
     * @param Event $event
     * @param Query $query
     * @param ArrayObject $options
     * @return null
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options)
    {
        if ($this->runTime()) {
            $query->where($this->createConditions());
        }
    }

    /**
     * createConditions
     *
     * @param bool $status
     * @return null
     */
    public function createConditions($status = true)
    {
        $config = $this->config();

        $fieldName = $config['field'];
        $fullFieldName = $this->_table->alias() .'.'.$fieldName;

        $statusName = ($status) ? 'enable' : 'disable';
        $fieldStatus = [];
        $fieldStatus[] = $config['status'][$statusName];
        if ($config['null'] === $statusName) {
            $fieldStatus[] = null;
        }

        $conditions = [];
        foreach ($fieldStatus as $f) {
            $conditions[] = [$fullFieldName . ' IS' => $f];
        }
        if (count($conditions) > 1) {
            $conditions = ['OR' => $conditions];
        }

        return $conditions;
    }

    /**
     * disable
     *
     * @param EntityInterface $entity
     * @param array $options
     * @return EntityInterface|false
     */
    public function disable(EntityInterface $entity, $options = [])
    {
        return $this->_statusChange($entity, $options, false);
    }

    /**
     * enable
     *
     * @param EntityInterface $entity
     * @param array $options
     * @return EntityInterface|false
     */
    public function enable(EntityInterface $entity, $options = [])
    {
        return $this->_statusChange($entity, $options, true);
    }

    /**
     * disableAll
     *
     * Disable all rows for disigned conditions.
     *
     * @param array $conditions
     * @return bool result
     */
    public function disableAll($conditions = null)
    {
        return $this->_updateAll(false, $conditions);
    }

    /**
     * enableAll
     *
     * Enabled all rows for disigned conditions.
     *
     * @param array $conditions
     * @return bool result
     */
    public function enableAll($conditions = null)
    {
        return $this->_updateAll(true, $conditions);
    }

    protected function _updateAll($status, $conditions = null)
    {
        if (!$this->hasField()) {
            return false;
        }
        $fields = $this->_createStatusField($status);

        $eventName = ($status) ? 'Enable' : 'Disable';
        $timestamps = $this->_getTimestampFields($this->_eventNamePrefix . '.before' . $eventName, 'Model.beforeSave');
        $ts = new Time();
        foreach ($timestamps as $timestamp) {
            $fields[$timestamp] = $ts;
        }

        return $this->_table->updateAll($fields, $conditions);
    }

    protected function _statusChange(EntityInterface $entity, array $options, $status)
    {
        $options = new ArrayObject($options + [
            'atomic' => true,
            'checkRules' => true,
        ]);

        if ($entity->errors()) {
            return false;
        }

        $connection = $this->_table->connection();
        if ($options['atomic']) {
            $success = $connection->transactional(function () use ($entity, $options, $status) {
                return $this->_processStatusChange($entity, (array)$options, $status);
            });
        } else {
            $success = $this->_processStatusChange($entity, (array)$options, $status);
        }

        if ($success) {
            if (!$connection->inTransaction() &&
                ($options['atomic'] || (!$options['atomic'] && $options['_primary']))
            ) {
                $method = ($status) ? '.afterEnableCommit' : '.afterDisableCommit';
                $this->_table->dispatchEvent($this->_eventNamePrefix.$method, compact('entity', 'options'));
            }
        }
        return $success;
    }

    protected function _processStatusChange(EntityInterface $entity, array $options, $status)
    {
        $primaryKey = (array)$this->_table->primaryKey();
        if (!$entity->has($primaryKey)) {
            $msg = 'Status switching requires all primary key values.';
            throw new InvalidArgumentException($msg);
        }

        $eventName = ($status) ? 'Enable' : 'Disable';

        $_entity = $this->_table->newEntity([]);
        $_entity->isNew(false);
        foreach ($primaryKey as $prop) {
            $_entity->set($prop, $entity->get($prop));
            $_entity->dirty($prop, false);
        }
        $fields = $this->_createStatusField($status);
        foreach ($fields as $prop => $val) {
            $_entity->set($prop, $val);
            $_entity->dirty($prop, true);
        }

        if ($options['checkRules'] && !$this->_table->checkRules($_entity, RulesChecker::UPDATE, $options)) {
            return false;
        }

        $event = $this->_table->dispatchEvent($this->_eventNamePrefix.'.before'.$eventName, [
            'entity' => $_entity,
            'options' => $options
        ]);

        if ($event->isStopped()) {
            return $event->result;
        }

        $runTime = $this->runTime();
        $this->runTime(false);

        $success = $this->_table->save($_entity, $options);

        if ($success) {
            $this->_cascadeStatusChange($_entity, $options, $status);
        }

        if ($success && !$options['atomic']) {
            $this->_table->dispatchEvent($this->_eventNamePrefix.'.after'.$eventName, [
                'entity' => $_entity,
                'options' => $options
            ]);
        }

        $this->runTime($runTime);

        $properties = $_entity->visibleProperties();
        foreach ($properties as $prop) {
            $entity->set($prop, $_entity->get($prop));
            $entity->dirty($prop, false);
        }

        if ($success) {
            return $entity;
        }
        return false;
    }

    protected function _createStatusField($status)
    {
        $config = $this->config();
        $fieldName = $config['field'];


        $fields = [];
        $fields[$fieldName] = $config['status'][$status ? ('enable') : ('disable')];

        return $fields;
    }

    protected function _cascadeStatusChange(EntityInterface $entity, array $options, $status)
    {
        $noCascade = [];
        $assocs = $this->_table->associations()->getIterator();
        foreach ($assocs as $assoc) {
            if (!$assoc->cascadeCallbacks()) {
                $noCascade[] = $assoc;
                continue;
            }
            $this->_cascadeStatusChangeByAssoc($assoc, $entity, $options, $status);
        }
        foreach ($noCascade as $assoc) {
            $this->_cascadeStatusChangeByAssoc($assoc, $entity, $options, $status);
        }
    }

    protected function _cascadeStatusChangeByAssoc(Association $assoc, EntityInterface $entity, array $options, $status)
    {
        $type = $assoc->type();
        if ($assoc->type() === Association::ONE_TO_ONE || $assoc->type() === Association::ONE_TO_MANY) {
            $this->_statusChangeDependent($assoc, $entity, $options, $status);
        } elseif ($assoc->type() === Association::MANY_TO_MANY) {
            $this->_statusChangeLink($assoc, $entity, $options, $status);
        }
    }

    protected function _statusChangeDependent(Association $assoc, EntityInterface $entity, array $options, $status) {
        if (!$assoc->dependent()) {
            return true;
        }
        if ($status) {
            $method = 'enable';
            $methodAll = 'enableAll';
        } else {
            $method = 'disable';
            $methodAll = 'disableAll';
        }
        if (!$assoc->behaviors()->hasMethod($method)
            && !$assoc->behaviors()->hasMethod($methodAll)) {
            return false;
        }
        $table = $assoc->target();
        $foreignKey = (array)$assoc->foreignKey();
        $bindingKey = (array)$assoc->bindingKey();
        $conditions = array_combine($foreignKey, $entity->extract($bindingKey));

        if ($assoc->cascadeCallbacks()) {
            foreach ($assoc->find()->where($conditions)->toList() as $related) {
                $table->$method($related, $options);
            }
            return true;
        }
        $conditions = array_merge($conditions, $assoc->conditions());
        return $table->$methodAll($conditions);
    }

    protected function _statusChangeLink(Association $assoc, EntityInterface $entity, array $options, $status) {
        if (!$assoc->dependent()) {
            return true;
        }
        if ($status) {
            $method = 'enable';
            $methodAll = 'enableAll';
        } else {
            $method = 'disable';
            $methodAll = 'disableAll';
        }
        $table = $assoc->junction();

        if (!$table->behaviors()->hasMethod($method)
            && !$table->behaviors()->hasMethod($methodAll)) {
            return false;
        }
        $foreignKey = (array)$assoc->foreignKey();
        $bindingKey = (array)$assoc->bindingKey();
        $conditions = [];

        if (!empty($bindingKey)) {
            $conditions = array_combine($foreignKey, $entity->extract($bindingKey));
        }

        $hasMany = $assoc->source()->association($table->alias());
        if ($assoc->cascadeCallbacks()) {
            foreach ($hasMany->find('all')->where($conditions)->toList() as $related) {
                $table->$method($related, $options);
            }
            return true;
        }

        $conditions = array_merge($conditions, $hasMany->conditions());
        return $table->$methodAll($conditions);
    }

    protected function _configWrite($key, $value, $merge = false)
    {
        parent::_configWrite($key, $value, $merge);

        $config = $this->_config;

        $status = $config['status'];

        if(!is_array($status)) {
            $status = [
                'enable' => $status,
                'disable' => !$status,
            ];
        } elseif (isset($status['enable']) || isset($status['disable'])) {
            if (!isset($status['enable'])) {
                $status['enable'] = !$status['disable'];
            }
            if (!isset($status['disable'])) {
                $status['disable'] = !$status['enable'];
            }
        } elseif (count($status) === 2) {
            $status = [
                'enable' => array_shift($status),
                'disable' => array_shift($status),
            ];
        } else {
            $status = $this->_defaultConfig['status'];
        }

        $config['status'] = $status;

        $config['null'] = strtolower($config['null']);

        $this->_config = $config;
    }
}
