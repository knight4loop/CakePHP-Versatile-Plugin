<?php return [
    'Versatile' => [
        'ImportShell' => [
            'engine' => 'yaml',
            'path' => ROOT . DS . 'datas' . DS . 'tables' . DS,
        ],
        'CacheClearShell' => [
            'exclude' => 'empty',
        ],
        'UserSeedShell' => [
            'userModel' => 'Users',
        ],
        'Labels' => [
            'path' => ROOT . DS . 'datas' . DS . 'tables' . DS . 'label.yaml',
        ],
        'Constants' => [
            'path' => ROOT . DS . 'datas' . DS . 'constant' . DS,
            'file' => CONFIG . 'constant.php',
        ],
    ]
];
