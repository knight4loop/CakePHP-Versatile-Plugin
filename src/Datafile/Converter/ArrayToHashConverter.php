<?php
namespace Versatile\Datafile\Converter;

class ArrayToHashConverter implements \Versatile\Datafile\ConverterInterface
{
    public static function exec($data)
    {
        foreach ($data as $idx => $value) {
            if (!is_array($value)) {
                $data[$idx] = [$idx => $value];
            }
            $cnt = count($value);
            if ($cnt < 1) {
                $data[$idx] = [$idx => ''];
                continue;
            }
            if ($cnt === 1) {
                continue;
            }
            $k = array_shift($value);
            $v = array_shift($value);
            $data[$idx] = [$k => $v];
        }

        return $data;
    }


}

