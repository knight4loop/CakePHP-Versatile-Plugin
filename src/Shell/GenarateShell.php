<?php
/**
 * ImportShell
 *
 * @package
 * @copyright
 * @author k.naito <k.naito@coosy.co.jp>
 */
namespace Versatile\Shell;

use Cake\Core\Configure;
use Cake\Utility\Inflector;
use Versatile\Datafile\Datafile;
use Versatile\Console\VersatileShell;

class GenarateShell extends VersatileShell
{
    public $tasks = ['Versatile.Constants'];

    /**
     * Gets the option parser instance and configures it.
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        foreach ($this->_taskMap as $task => $config) {
            $taskParser = $this->{$task}->getOptionParser();
            $parser->addSubcommand(Inflector::underscore($task), [
                'help' => $taskParser->description(),
                'parser' => $taskParser
            ]);
        }

        return $parser;
    }

    public function startup ()
    {
        parent::startup();
        $this->Datafile = new Datafile();
    }

}
