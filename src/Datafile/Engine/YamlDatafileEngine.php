<?php
namespace Versatile\Datafile\Engine;

use Symfony\Component\Yaml\Yaml;
use Versatile\Datafile\Engine\BaseDatafileEngine;
use Versatile\Datafile\TextFileDatafileTrait;

class YamlDatafileEngine extends BaseDatafileEngine
{
    use TextFileDatafileTrait;

    public function read($sheet = null)
    {
        $path = $this->_getTextFilePath($sheet);

        $this->path($path);

        $contents = Yaml::parse(file_get_contents($path));

        return $contents;
    }
}
