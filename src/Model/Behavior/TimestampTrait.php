<?php
namespace Versatile\Model\Behavior;

trait TimestampTrait
{
    protected $_TimestampBehaviorName = 'Timestamp';

    protected function _getTimestampFields($eventName = 'Model.beforeSave')
    {
        $fields = $this->config('timestamp');
        if (empty($fields)) {
            $fields = [];
        }
        if (!is_array($fields)) {
            $fields = [$fields];
        }

        if (!empty($eventName)) {
            $TimestampBehaviorName = $this->config('TimestampBehavior');
            if (empty($TimestampBehaviorName)) {
                $TimestampBehaviorName = $this->_TimestampBehaviorName;
            }

            if ($this->_table->hasBehavior($TimestampBehaviorName)) {
                $TimestampBehavior = $this->_table->behaviors()->get('Timestamp');
                $events = $TimestampBehavior->config('events');

                if (!is_array($eventName)) {
                    $eventName = func_get_args();
                }

                foreach ($eventName as $event) {
                    if (!isset($events[$event])) {
                        continue;
                    }
                    foreach ($events[$event] as $field => $when) {
                        if (in_array($when, ['always', 'existing'])
                            && $this->_table->hasField($field)) {
                            $fields[] = $field;
                        }
                    }
                }
            }
        }
        $fields = array_unique($fields);

        return $fields;
    }
}
