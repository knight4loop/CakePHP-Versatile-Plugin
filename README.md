# Versatile plugin for CakePHP

## Installation

You can install this plugin into your CakePHP application using [composer](http://getcomposer.org).

The recommended way to install composer packages is:

```
composer require coosy/Versatile
```

## 追加される Shell コマンド
- Versatile.generate_constants_shell

Excelファイルから定数クラスを生成します。(doc/initial_data.xlsx, doc/constants.xlsx)

- Versatile.generate_security_seed

core.php に記載するキーをランダムで生成します。

- Versatile.generate_validation_message

Excelファイルからメッセージコンフィグファイルを作成します。(doc/validation_messages.xlsx)

- Versatile.initial_import

初期データを投入します(総入れ替え)。(doc/initial_import_.xlsx)

- Versatile.test_data_export

テスト用データをエクスポートします。(doc/test_data.xlsx)

- Versatile.test_data_import

テスト用データをDBにインポートします。(doc/test_data.xlsx)

## Component
- ProtocolHandler

SSL 表示強制コンポーネントです。

- デフォルトではコントローラーの forceSSL プロパティを参照します。
	- true で全メソッドに対して、配列形式だと特定のメソッドに対して SSL を強制します。

```php
class SecureController extends AppController {
	public $forceSSL = true;	// true or array(methods)
	public $components = array(
		"Versatile.ProtocolHandler",
	);
}
class PartSecureController extends AppController {
	public $forceSSL = array("create", "add");
	public $components = array(
		"Versatile.ProtocolHandler",
	);
}
```

- SSL がデフォルトで部分的に HTTP で表示させることも可能です。

```php
class InsecureController extends AppController {
	public $forceHTTP = array("create", "add");
	public $components = array(
		"Versatile.ProtocolHandler" => array(
			"defaultProtocol" => "https",
			"propertyName" => "forceHTTP",
			"auto" => true,
		),
	);
}
```

- 自動で動作するのをストップさせたい場合は auto プロパティに false を指定してください

```php
class ManualController extends AppController {
	public $components = array(
		"Versatile.ProtocolHandler" => array(
			"auto" => false,
		),
	);

	public function beforeFilter() {
		if ($this->ProtocolHandler->isSSL() && $this->request->is('ajax')) {
			$this->ProtocolHandler->forceSSL();
		} else {
			$this->ProtocolHandler->unforceSSL();
		}
	}
}
```

- 使用している環境に SSL を導入していない場合は、以下の設定を追加してください。

```
Configure::write('ProtocolHandler.nossl', 1);
```

## Behavior
- OptimisticLock

楽観的排他ロック機能を提供します。

- VersatileAutoConvert

値の自動変換機能を提供します。

- VersatileCondition

Search.Prg 用の条件生成メソッドを提供します。

- VersatileCryptField

特定のフィールドを暗号化する機能を提供します。
情報量が元のデータに比べて増加するため、データベースのカラム長には気をつける必要があります。

- VersatileFind

検索用の基本的なメソッドを提供します。

- VersatileMessageFormat

Excelファイルでのエラーメッセージ機能を提供します。
ただし、メッセージの更新はシェルコマンドで実施する必要があります。

- VersatileJoinHelper

join に必要な Array を返却します。定義はモデルに記述されている `hasOne`, `belongsTo`, `hasMany` を参照して作成します。

```php
public function listAvailablesByUserId($user_id) {
	$alias = $this->alias;
	return $this->find('list', array(
		"fields" => array(
			"$alias.id", "Coupon.value",
		),
		"conditions" => array(
			"$alias.user_id" => $user_id,
			"$alias.is_disabled" => 0,
		),
		"joins" => array(
			$this->makeInnerJoin("Coupon"),
		),
	));
}
```

- VersatileValidations

いくつかのバリデーション用メソッドを提供します。

## View
- MobileTwigView

フィーチャーフォン、スマートフォン向けのテンプレート切り替え機能を提供します。
TwigViewプラグインが必要です。

- VersatileView

通常のViewクラスにない機能を提供します。

### partial

同一フォルダの '_' のついたテンプレートファイルを読み込みます。
第二引数に配列を渡すことで、テンプレートに変数を渡せます。

```ctp
<?= $this->partial('parts_file_name'); ?>

<?= $this->partial('parts_file_name', ['type'=>'default']); ?>
```

### spanlessStart/spanlessEnd

spanlessStart は spanlessEnd が呼び出されるまでに記述された内容をスタックします。
spanlessEnd はスタックした内容から、先頭の空白文字(タブ、ホワイトスペース)と
すべてのタブと改行を除いた内容を返します。
文字の間にある空白は残ります。

```ctp
<?= $this->spanlessStart(); ?>
<p>
	<span>
		remove spaces.
	</span>
</p>
<?= $this->spanlessEnd ?>
<?php /*
# 出力値
<p><span>remove spaces.</span></p>
*/ ?>
```

### spanless

渡された内容から、先頭の空白文字(タブ、ホワイトスペース)と
すべてのタブと改行を除いた内容を返します。
文字の間にある空白は残ります。

```ctp
<?= $this->spanless("<p>\n\t<span>\n\t\tremove spaces.\n\t</span>\n</p>"); ?>
<?php /*
# 出力値
<p><span>remove spaces.</span></p>
*/ ?>
```

## View/Helper
- Label

項目関連のフォーム作成機能を提供します。

### 記述例
```twig
#### Show
{{ label.is_deleted(1) }}
{{ label.gender(1) }}

#### Radio
{{ label.radio('is_deleted')|raw }}

{{ label.radio('form_name', "is_deleted", {
})|raw }}

{{ label.radio('is_deleted', null, {
})|raw }}

{% set list = label.selectArray('is_deleted') %}
{% for key, val in list %}
{{ form.radio("is_deleted", { (key) : val })|raw }}{# 括弧をつけないと "key" として処理されてしまうので注意 #}
{% endfor %}

#### Select
{{ label.select('is_deleted', null, {
	empty: "選択してください",
})|raw }}
{{ label.select('form_name', "is_deleted", {
})|raw }}

#### Checkbox
{% for val, desc in label.selectArray("is_deleted") %}
{{ form.checkbox("is_deleted.", {
	value: val,
})|raw }} {{ desc }}
{% endfor %}
```


### 出力結果
```html
#### Show
削除済み
男性

#### Radio
<input type="hidden" name="data[Item][is_deleted]" id="ItemIsDeleted_" value=""/><input type="radio" name="data[Item][is_deleted]" id="ItemIsDeleted0" value="0" required="required" />-<input type="radio" name="data[Item][is_deleted]" id="ItemIsDeleted1" value="1" required="required" />削除済み

<input type="hidden" name="data[Item][form_name]" id="ItemFormName_" value=""/><input type="radio" name="data[Item][form_name]" id="ItemFormName0" value="0" />-<input type="radio" name="data[Item][form_name]" id="ItemFormName1" value="1" />削除済み

<input type="hidden" name="data[Item][is_deleted]" id="ItemIsDeleted_" value=""/><input type="radio" name="data[Item][is_deleted]" id="ItemIsDeleted0" value="0" required="required" />-<input type="radio" name="data[Item][is_deleted]" id="ItemIsDeleted1" value="1" required="required" />削除済み

<input type="hidden" name="data[Item][is_deleted]" id="ItemIsDeleted_" value=""/><input type="radio" name="data[Item][is_deleted]" id="ItemIsDeletedKey" value="0" required="required" />-
<input type="hidden" name="data[Item][is_deleted]" id="ItemIsDeleted_" value=""/><input type="radio" name="data[Item][is_deleted]" id="ItemIsDeletedKey" value="1" required="required" />削除済み

#### Select
<select name="data[Item][is_deleted]" id="ItemIsDeleted" required="required">
<option value="">選択してください</option>
<option value="0">-</option>
<option value="1">削除済み</option>
</select>

<select name="data[Item][form_name]" id="ItemFormName">
<option value=""></option>
<option value="0">-</option>
<option value="1">削除済み</option>
</select>

#### Checkbox
<input type="checkbox" name="data[Item][is_deleted][]"  value="0" id="ItemIsDeleted"/> -
<input type="checkbox" name="data[Item][is_deleted][]"  value="1" id="ItemIsDeleted"/> 削除済み
```

- SimpleForm

スタイルを極力付加しないFormHelper機能を提供します。
標準のFormHelper では checkbox で value = 0 を指定できないのですが、指定できるよう改修済みです。
エラーのスタイルだけ特別扱いしており、オプション指定が可能です。

```php
public $helpers = array(
	'Form' => array(
		"className" => "Versatile.SimpleForm",
		"errorDefaults" => array("wrap" => "span", "class" => "red")	// エラー時の option のデフォルト
	),
);
```

- SimpleXFormjp

スタイルを極力付加しないFormHelper機能を提供します。
標準のFormHelper では checkbox で value = 0 を指定できないのですが、指定できるよう改修済みです。
XFormjp クラスを継承しているため、確認画面の構築に向いています。
Cakeplus プラグインが必要です。
エラーのスタイルだけ特別扱いしており、オプション指定が可能です。

```php
public $helpers = array(
	'Form' => array(
		"className" => "Versatile.SimpleXformjp",
		"errorDefaults" => array("wrap" => "span", "class" => "red")	// エラー時の option のデフォルト
	),
);
```

- ImageHelper

画像をリサイズします。
gd 拡張が必要です。

```php
public $helpers = array(
	"Versatile.Image" => array(
		"cacheDir" => "resized_images",	// 画像生成先ディレクトリ(webroot からの相対パス)
	),
);
```


```
{# 横=32px, 縦=50px の画像にリサイズ #}
{{ image.resize("fish_tank.jpg", 32, 50, {
	alt : "fish",
})|raw }}
```

```
<img src="/resized_images/32x50/a0429/fish_tank.jpg.jpeg" alt="fish" />
```

## Lib
- EnvUtil

環境切り替え機能を提供します。
本番・テスト環境のファイルのロードに利用が可能です。
設定値の切り替えは環境変数 `CAKE_ENV` にて行われます。

``` Config/database.php
<?php
App::uses('EnvUtil', 'Versatile.Lib');

EnvUtil::loadFile(__FILE__);
// CAKE_ENV = "development" の場合 Config/development/database.php が読み込まれます。
// Config/development/database.php が存在しない場合、Config/development/database.php.default を読み込みにいきます。
```

- ConfigManager

設定情報を管理するための基本的な機能を提供します。
設定情報は PHP のクラス名か、もしくは ini ファイルで設定することが可能です。
詳しい使用方法はテストケースを参照のこと。

## Configure
- Configure::write を使用することでいくつかの設定を変更することができます。
- Lib/VersatileConfigUtil.php で設定の読み込みを行っているので、どのような値が設定できるかはこちらのクラスを参照してください。

## TODO
----

- doc
- phpdoc
- testcase
