<?php
namespace Versatile\Datafile;

use Cake\Filesystem\Folder;

trait TextFileDatafileTrait
{
    public function path($path = null)
    {
        if ($path === null) {
            return $this->_path;
        }

        $path = realpath($path);


        if (is_file(realpath($path))) {
            $this->_path = dirname($path);
            $this->sheet(basename($path));
        } else {
            $this->_path = $path;
            $this->sheet('');
        }

        return $this;
    }

    public function sheets()
    {
        $sheet = $this->sheet();
        if ($sheet) {
            return [$sheet];
        }

        $folder = new Folder($this->path());
        $sheets = $folder->find('.*', true);

        return $sheets;
    }

    protected function _getTextFilePath($key)
    {
        $path = null;
        $sheet = $this->sheet();

        if (!$sheet) {
            $sheet = $key;
        }

        if (!$this->hasSheet($sheet)) {
            throw new Exception(sprintf('Not found sheet %s', $sheet));
        }

        if ($path === null) {
            $path = $this->path();
        }

        $path = Folder::slashTerm($path) . $sheet;

        if (!$path || !is_file(realpath($path))) {
            throw new Exception(sprintf('Could not read file: %s', $path));
        }

        return $path;
    }
}
