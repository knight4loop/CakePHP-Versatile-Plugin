<?php
namespace Versatile\Model\Behavior;

use ArrayObject;
use Cake\Core\Exception;
use Cake\Collection\CollectionInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\ORM\Behavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Utility\Text;
use Cake\Utility\Hash;
use Psr\Log\LogLevel;
use Versatile\Model\Behavior\Exception\OptimisticLockException;
use Versatile\Log\LogTrait;

class OptimisticLockBehavior extends Behavior
{
    use TimestampTrait;
    use LogTrait {
        LogTrait::logStart as protected _logStart;
        LogTrait::logEnd as protected _logEnd;
    }

    protected $_defaultConfig = [
        'implementedFinders' => [
            'lock' => 'findLock',
        ],
        'implementedMethods' => [
            'optimisticLock' => 'lock',
            'isOptimisticLocked' => 'isLocked',
        ],
        'version' => 'version',        // column name
        'error' => array(
            'handler' => 'message', // message or exception
            'message' => 'Update conflict, another user has already updated the record. Please list and edit the record again.',    // error message
        ),
        'timestamp' => null,
        'auto' => true,
        'single' => false,
    ];

    protected $_stop = false;

    public function beforeFind(Event $event, Query $query, ArrayObject $options) {
        if ($this->_stop || !$this->config('auto')) {
            return;
        }
        $mapper = function ($entity, $key, $mapReduce) {
            $mapReduce->emitIntermediate($entity, 'lock');
        };
        $reducer = function ($entries, $key, $mapReduce) {
            $this->lock($entries);
            foreach ($entries as $entity) {
                $mapReduce->emit($entity);
            }
        };
        $query->mapReduce($mapper, $reducer);
    }

    public function beforeRules(Event $event, EntityInterface $entity, $options, $operation)
    {
        if (!$this->config('auto')) {
            return;
        }

        if (!$this->isLocked($entity)) {
            return false;
        }
    }

    public function findLock(Query $query, array $options)
    {
        return $query->formatResults(function ($results) use ($options) {
            $this->lock($results);
            return $results;
        });
    }

    public function isLocked($entity)
    {
        $this->logStart();

        $this->_stop = true;

        $version = $this->config('version');
        $primaryKey = (array)$this->_table->primaryKey();

        if ((!$this->_table->hasField($version) && !$entity->hasField($primaryKey))
            || $entity->isNew()
        ) {
            $this->log("Check version field : Nothing to do.", LOG_DEBUG);
            $this->logEnd();
            $this->_stop = false;
            return true;
        }

        $where = [];
        foreach ($primaryKey as $p) {
            $where[] = $entity->get($p);
        }

        $row = $this->_table
            ->find('all')
            ->where($where)
            ->first();

        $success = true;
        if (isset($row->$version)) {
            $success = $entity->$version === $row->$version;
            $this->log(sprintf(
                "Check version field : %s.%s : ([%s] <=> [%s]) : %s",
                $this->_table->table(),
                $version,
                $entity->$version,
                $row->$version,
                $success ? 'Success' : 'Faild'
            ), LOG_DEBUG);

            if (!$success) {
                $error = $this->config('error');
                if ($error["handler"] == "exception") {
                    throw new OptimisticLockException($error["message"]);
                } else {
                    $entity->errors($version, [$error["message"]]);
                }
            }
        } else {
            $this->log(sprintf(
                "Check version field : %s.%s : Empty version field.",
                $this->_table->table(),
                $version
            ), LOG_DEBUG);
        }

        $this->_stop = false;

        $this->logEnd();
        return $success;
    }

    public function lock($entries)
    {
        $this->logStart();
        if ($entries instanceof EntityInterface) {
            $_entries = [$entries];
        } elseif ($entries instanceof CollectionInterface) {
            $_entries = $entries;
        } else {
            $_entries = (array)$entries;
        }

        $version = $this->config('version');
        $single = $this->config('single');

        if (!$this->_table->hasField($version) || ($single && count($_entries) != 1)) {
            $this->log("Lock entries : Nothing to do.", LOG_DEBUG);
            $this->logEnd();
            return false;
        }

        $primaryKey = (array)$this->_table->primaryKey();

        $uuid = Text::uuid();
        $timestamp = new Time();
        $timestampFields = $this->_getTimestampFields();

        $ids = [];
        foreach ($_entries as $entity) {
            if (!$entity->has($primaryKey)) {
                $msg = 'Lock requires all primary key values.';
                throw new InvalidArgumentException($msg);
            }
            $entity->set($version, $uuid);
            $entity->dirty($version, false);
            foreach ($timestampFields as $tf) {
                $entity->set($tf, $timestamp);
                $entity->dirty($tf, false);
            }
            $p = [];
            foreach ($primaryKey as $k) {
                $p[$k . ' IS'] = $entity->get($k);
            }
            $ids[] = $p;
        }

        $fields = [];
        $fields[$version] = $uuid;
        foreach ($timestampFields as $tf) {
            $fields[$tf] = $timestamp;
        }

        $success = $this->_table->updateAll($fields, ['OR' => $ids]);

        if ($success) {
            $this->log("Lock entries : Locked ->", LOG_DEBUG);
            $success = $entries;
        } else {
            $this->log("Lock entries : Can not locked ->", LOG_DEBUG);
        }
        $this->log($ids, LOG_DEBUG);
        $this->log($fields, LOG_DEBUG);

        $this->logEnd();
        return $success;
    }

    public function logStart($msg = null, $level = LogLevel::DEBUG, $context = [])
    {
        $this->_logStart($msg, $level, $context);
    }

    public function logEnd($msg = null, $level = LogLevel::DEBUG, $context = [])
    {
        $this->_logEnd($msg, $level, $context);
    }

}
