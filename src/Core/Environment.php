<?php
/**
 * 環境設定
 *
 */
namespace Versatile\Core;

use Cake\Core\Configure;

class Environment
{
    static protected $_envName = 'CAKE_ENV';

    static protected $_defaultName = 'production';

    static protected $_defaultEnvFilePath = CONFIG . 'environment';

    /**
     * 環境名を返却また設定します。
     * @return string
     */
    static public function name($name = null)
    {
        if ($name !== null) {
            putenv(static::envName().'='.$name);
        }
        return env(static::envName(), static::$_defaultName);
    }

    /**
     * 環境名を保存している環境名を返却または設定します。
     *
     * @return string
     */
    static public function envName($envName = null)
    {
        if ($envName !== null) {
            static::$_envName = $envName;
        }
        return static::$_envName;
    }

    static public function setByTextFile($path = null)
    {
        if ($path === null) {
            $path = static::$_defaultEnvFilePath;
        }

        if (\file_exists($path)) {
            $name = str_replace(["\r", "\n"], '', file_get_contents($path));
        } else {
            $name = static::name();
        }
        $name = static::name($name);

        return $name;
    }

    /**
     * 環境ごとの設定ファイルを読み込みます。
     *
     * @return boolen
     */
    static public function load($config = [], $engine = 'default', $merge = true)
    {
        if (!$engine) {
            return false;
        }

        if (!empty($config) && is_string($config)) {
            $env = static::name($config);
        } else if(env(static::envName()) === null) {
            if (!is_array($config)) {
                $config = [];
            }
            $config += [
                'path' => null,
            ];
            $env = static::setByTextFile($config['path']);
        } else {
            $env = static::name();
        }

        try {
            $res = Configure::load($env, $engine, $merge);
        } catch (\Exception $e) {
            try {
                $res = Configure::load($env.'.default', $engine, $merge);
            } catch (\Exception $e) {
                $res = false;
            }
        }
        return $res;
    }
}
