CREATE TABLE IF NOT EXISTS labels
(
	id int unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
	category text COMMENT 'category',
	label text COMMENT 'label',
	value int COMMENT 'value',
	created datetime NOT NULL COMMENT 'created',
	modified datetime NOT NULL COMMENT 'modified',
PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '選択項目テーブル' DEFAULT CHARACTER SET utf8;
