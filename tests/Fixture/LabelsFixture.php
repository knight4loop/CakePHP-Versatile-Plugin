<?php
/**
 * LabelFixture
 *
 */
namespace Versatile\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

class LabelsFixture extends TestFixture
{

/**
 * Fields
 *
 * @var array
 */
    public $fields = [
        'id' => ['type' => 'integer'],
        'category' => ['type' => 'text'],
        'label' => ['type' => 'text'],
        'value' => ['type' => 'integer'],
        'created' => ['type' => 'datetime', 'null' => false],
        'modified' => ['type' => 'datetime', 'null' => false],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id']]
        ]
    ];

/**
 * Records
 *
 * @var array
 */
    public $records = array(
        array(
            'id' => 1,
            'category' => 'status',
            'label' => '編集済み',
            'value' => 0,
            'created' => '2014-03-03 15:44:05',
            'modified' => '2014-03-03 15:44:05',
        ),
        array(
            'id' => 2,
            'category' => 'status',
            'label' => '採用',
            'value' => 1,
            'created' => '2014-03-03 15:44:05',
            'modified' => '2014-03-03 15:44:05',
        ),
        array(
            'id' => 3,
            'category' => 'status',
            'label' => '不採用',
            'value' => 9,
            'created' => '2014-03-03 15:44:05',
            'modified' => '2014-03-03 15:44:05',
        ),
    );

}
