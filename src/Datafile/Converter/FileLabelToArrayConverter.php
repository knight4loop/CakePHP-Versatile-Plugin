<?php
namespace Versatile\Datafile\Converter;

class FileLabelToArrayConverter implements \Versatile\Datafile\ConverterInterface
{
    public static function exec($data)
    {
        $_data = [];
        foreach ($data as $idx => $value) {
            if (is_numeric($idx)) {
                $_data[] = $value;
            } elseif (is_array($value)) {
                foreach ($value as $_idx => $_row) {
                    $_row['category'] = $idx;
                    $_data[] = $_row;
                }
            }
        }
        return $_data;
    }

}
