<?php
/*
 * パスワード生成コンポーネント
 *
 * author k.naito@coosy.co.jp
 */
namespace Versatile\Utility;

class PasswordGenerator{
    /**
     * @var string キーワード生成に使用する文字列
     */
    protected static $_lowerCase = 'abcdefghijklnmopqrstuvwxyz';
    protected static $_upperCase = 'ABCDEFGHIJKLNMOPQRSTUVWXYZ';
    protected static $_numeric   = '0123456789';
    protected static $_sign      = '!#$%&*+@?';

    /**
     * @var array 使用できるパスワードタイプ指定
     */
    protected static $_allowTypes = array(
        'small', 'large', 'alphabet', 'number', 'sign',
    );

    /**
     * @var array デフォルトの設定値
     */
    protected static $_config = [
        'types' => [
            'alphabet', 'number'
        ],
        'length' => 8
    ];

    /**
     * デフォルトの設定値を変更します。
     *
     * @param array $config
     * @return null
     * */
    static public function config($config = null)
    {
        if (!is_array($config)) {
            $c = func_get_args();
            $config = [];
            if (isset($c[0])) {
                $config['length'] = $c[0];
            }
            if (isset($c[1])) {
                $config['types'] = $c[1];
            }
        }
        /* デフォルト設定を行う */
        if(isset($config['types'])){
            static::$_config['types'] = static::_types($config['types']);
        }
        if(isset($config['length'])){
            static::$_config['length'] = static::_length($config['length']);
        }
    }

    /**
     * 使用可能なタイプかどうかを検証します。
     *
     * @param string $val 検証したいタイプ文字列
     * @return boolen
     * */
    static public function isAllowType($val){
        return in_array($val, static::$_allowTypes);
    }


    static private function _types($types)
    {
        $type = static::_filterTypes($types);
        if(empty($type)){
            $type = static::$_config['types'];
        }

        return $type;
    }

    static private function _length($length)
    {
        $length = static::_checkLength($length);
        if($length < 1){
            $length = static::$_config['length'];
        }
        return $length;
    }

    /**
     * パスワードを生成します
     *
     * @param int|null $length 文字列長
     * @param string|array|null $types 生成するパスワードの種類
     * */
    static public function create($length = 0, $types = null)
    {
        $length = static::_length($length);
        $types = static::_types($types);

        $chars = '';
        foreach($types as $key=>$type){
            $chars .= static::_getChars($type);
        }
        $pin = '';
        $maxCharcterLength = strlen($chars);
        for($i = 0; $i < $length; $i++){
            $seed = static::_makeSeed($maxCharcterLength, $chars);
            $pin .= substr($chars, $seed, 1);
        }
        return $pin;
    }

    /**
     * 乱数を作成します
     *
     * @param int $maxlength 文字列長
     * */
    static protected  function _makeSeed($maxlength, $chars = null){
        return mt_rand(0, $maxlength - 1);
    }

    /**
     * 指定したタイプから使用できるタイプだけにするようにフィルタリングします。
     *
     * @param string|array $types 検証したいタイプ文字列もしくは配列
     * @return boolen
     * */
    static protected function _filterTypes($types = null){
        $res = array();
        if(is_string($types)){
            $types = array($types);
        }
        if(is_array($types)){
            $res = array_filter($types, [__CLASS__, 'isAllowType']);
        }
        return $res;
    }

    /**
     * 有効な文字列長かどうかを検証します。
     *
     * @param int $length 文字列長
     * @return int
     * */
    static protected function _checkLength($length = 0){
        $res = 0;
        if(is_string($length)){
            $length = intval($length);
        }
        if(is_int($length) && 0 < $length){
            $res = $length;
        }
        return $res;
    }

    /**
     * 使用する文字列を返します。
     *
     * @param string $type 使用する文字列のタイプ
     * @return string
     * */
    static protected function _getChars($type){
        $chars = '';
        switch($type){
        case 'small':
            $chars = static::$_lowerCase;
            break;
        case 'large':
            $chars = static::$_upperCase;
            break;
        case 'alphabet':
            $chars = static::$_lowerCase . static::$_upperCase;
            break;
        case 'number' :
            $chars = static::$_numeric;
            break;
        case 'sign':
            $chars = static::$_sign;
        }
        return $chars;
    }

}
