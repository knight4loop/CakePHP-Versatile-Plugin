<?php
namespace Versatile\Datafile\Engine;

use Cake\Exception\Exception;
use Cake\Filesystem\Folder;

abstract class BaseDatafileEngine
{
    protected $_sheet = '';

    abstract function path($path = null);

    public function sheet($sheet = null)
    {
        if ($sheet === null) {
            return $this->_sheet;
        }

        if ($sheet !== '' && !$this->hasSheet($sheet)) {
            throw new Exception(sprintf('not found sheet %s', $sheet));
        }

        $this->_sheet = $sheet;

        return $this;
    }

    public function hasSheet($sheet)
    {
        $sheets = $this->sheets();
        return in_array($sheet, $sheets);
    }

    abstract public function sheets();

    abstract public function read($sheet = null);

    public function clear()
    {
        $this->_path = '';
        $this->_sheet = '';
        return $this;
    }
}
