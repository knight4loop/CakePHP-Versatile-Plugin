<?php
namespace Versatile\Test\TestCase\Controller\Component;

use Cake\TestSuite\TestCase;
use Cake\Controller\Controller;
use Cake\Controller\ComponentRegistry;
use Cake\Network\Request;
use Cake\Network\Response;
use Versatile\Core\GlobalObjectStrage;
use Versatile\Controller\Component\AuthComponent;

class AuthComponentTest extends TestCase
{
    public $controller = null;
    public $Auth = null;

    public function setUp()
    {
        parent::setUp();
        // コンポーネントと偽のテストコントローラのセットアップ
        $request = new Request();
        $response = new Response();
        $this->controller = $this->getMock(
            'Cake\Controller\Controller',
            null,
            [$request, $response]
        );
        $registry = new ComponentRegistry($this->controller);
        $this->Auth = new AuthComponent($registry);
    }

    public function testInitialize()
    {
        $Auth = GlobalObjectStrage::get('AuthComponent');
        $this->assertEquals($this->Auth, $Auth);

        GlobalObjectStrage::remove('AuthComponent');
        $this->assertNotEmpty($this->Auth);

        GlobalObjectStrage::set('AuthComponent', $this->Auth);
        $Auth = GlobalObjectStrage::get('AuthComponent');
        $this->assertEquals($this->Auth, $Auth);
    }
}
