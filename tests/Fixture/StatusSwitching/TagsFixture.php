<?php
namespace Versatile\Test\Fixture\StatusSwitching;

use Cake\TestSuite\Fixture\TestFixture;

class TagsFixture extends TestFixture
{
    /**
     * fields property
     *
     * @var array
     */
    public $fields = [
        'id' => ['type' => 'integer', 'key' => 'primary'],
        'tag' => ['type' => 'string', 'null' => false],
        'is_disabled' => ['type' => 'integer', 'null' => true, 'default' => 0],
        'disabled' => ['type'=>'datetime', 'null' => true],
        'created' => ['type' => 'datetime', 'null' => true, 'default' => null],
        'updated' => ['type' => 'datetime', 'null' => true, 'default' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id']]
        ]
    ];

    /**
     * records property
     *
     * @var array
     */
    public $records = [
        ['tag' => 'tag1', 'created' => '2007-03-18 12:22:23', 'updated' => '2007-03-18 12:24:31'],
        ['tag' => 'tag2', 'created' => '2007-03-18 12:24:23', 'updated' => '2007-03-18 12:26:31'],
        ['tag' => 'tag3', 'created' => '2007-03-18 12:26:23', 'is_disabled' => 1, 'disabled' => '2007-03-18 12:28:31', 'updated' => '2007-03-18 12:28:31']
    ];
}
