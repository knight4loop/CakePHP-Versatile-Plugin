<?php
namespace Versatile\Shell;

use Cake\Filesystem\Folder;
use Cake\Core\Configure;
use Versatile\Console\VersatileShell;

class CacheClearShell extends VersatileShell
{

    /**
     * Gets the option parser instance and configures it.
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->description(
            "Cache フォルダ内のファイルを再帰的に削除します。"
        )->addOption('exclude', [
            'help' => "指定した名前のファイルを削除から除外します。複数指定する場合は「,」を使用してください。",
            'default' => Configure::read('Versatile.CacheClearShell.exclude'),
            'short' => 'e'
        ]);

        return $parser;
    }

    public function main() {
        $this->_clearCakeCache();
        $this->hr();
        $this->out(__('Finish.'));
    }

    protected function _clearCakeCache(){
        $exclude = $this->param('exclude');
        $exclude = explode(',', $exclude);
        $exclude = (array)array_map(function($value)
        {
            return trim($value);
        }, $exclude);
        $exclude = array_unique($exclude);

        $dir = new Folder(CACHE);

        $this->out($dir->pwd());
        $this->out('Exclude file : ' . implode(' ', $exclude));
        $len = strlen($dir->pwd());

        $files = $dir->findRecursive();
        foreach ($files as $file) {
            if (in_array(basename($file), $exclude)) {
                continue;
            }
            $filename = substr($file, $len);
            if (unlink($file)) {
                $this->info('Success : ' . $filename);
            } else {
                $this->warning('Error : ' . $filename);
            }
        }
    }
}
