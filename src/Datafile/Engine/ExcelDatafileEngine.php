<?php
namespace Versatile\Datafile\Engine;

use Versatile\Utility\ExcelUtil;
use Versatile\Datafile\Engine\BaseDatafileEngine;

class ExcelDatafileEngine extends BaseDatafileEngine
{
    protected $_path = '';

    protected $_excelUtil = null;

    public function path($path = null)
    {
        if ($path === null) {
            return $this->_path;
        }

        $this->_path = realpath($path);
        $this->_excelUtil = ExcelUtil::open($path);

        return $this;
    }

    public function sheets()
    {
        return $this->_excelUtil->sheets;
    }

    public function read($sheet = null)
    {
        if ($sheet === null) {
            $sheets = $this->sheets();
            $sheet = current($sheets);
        }
        $contents = $this->_excelUtil->readSheet($sheet);
        return $contents;
    }

    public function clear()
    {
        parent::clear();

        unset($this->_excelUtil);

        return $this;
    }
}
