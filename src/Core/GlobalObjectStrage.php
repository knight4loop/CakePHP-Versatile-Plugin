<?php
namespace Versatile\Core;

use RuntimeException;

class GlobalObjectStrage
{
    static protected $_instances = [];

    static protected $_allowInstances = [
        'Cake\Controller\Component',
        'Cake\ORM\Behavior',
    ];

    static public function get($name)
    {
        if (!isset(static::$_instances[$name])) {
            throw new RuntimeException(sprintf(
                'You cannot get "%s", it not exists in the registry.',
                $name
            ));
        }
        return static::$_instances[$name];
    }

    static public function set($name, &$object)
    {
        if (!static::_isAllow($object)) {
            throw new RuntimeException(sprintf(
                'You cannot configure "%s", it not allow object instance in the registry.',
                $name
            ));
        }
        if (isset(static::$_instances[$name])) {
            if (static::$_instances[$name] !== $object) {
                throw new RuntimeException(sprintf(
                    'You cannot configure "%s", it already exists in the registry.',
                    $name
                ));
            }
        }
        static::$_instances[$name] = $object;
    }

    static public function keys()
    {
        return array_keys(static::$_instances);
    }

    static public function exists($name)
    {
        return isset(static::$_instances[$name]);
    }

    static public function remove($name)
    {
        unset(static::$_instances[$name]);
    }

    static public function clear()
    {
        static::$_instances = [];
    }

    static protected function _isAllow(&$object)
    {
        foreach (static::$_allowInstances as $instanceName) {
            if ($object instanceof $instanceName) {
                return true;
            }
        }
        return false;
    }
}
