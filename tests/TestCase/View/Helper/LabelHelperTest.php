<?php
namespace Versatile\Test\TestCase\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Cake\View\View;
use Cake\View\Helper\FormHelper;
use Versatile\View\Helper\LabelHelper;

/**
 * LabelHelper Test Case
 *
 */
class LabelHelperTest extends TestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = array('plugin.versatile.labels');

    protected $_expectedArray = array (
        0 => '編集済み',
        1 => '採用',
        9 => '不採用',
    );

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();

        //$this->Controller = new Controller($this->getMock('CakeRequest'), new CakeResponse());
        //$this->View = new View($this->Controller);
        $this->View = new View();
        $this->LabelHelper = new LabelHelper($this->View);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->LabelHelper);

        parent::tearDown();
    }

    /**
     * testLabelByValue method
     *
     * @return void
     */
    public function testMagicGet() {
        $v = $this->LabelHelper->status;
        $this->assertEquals($v, $this->_expectedArray, "magic get");
    }
    /**
     * testMagicCall method
     *
     * @return void
     */
    public function testMagicCall() {
        $v = $this->LabelHelper->status(0);

        $this->assertEquals($v, "編集済み", "magic call");
    }
    /**
     * testValueByLabel method
     *
     * @return void
     */
    public function testValueByLabel() {
        $v = $this->LabelHelper->valueByLabel("status", "編集済み");
        $this->assertTrue($v == 0, "normal");

        $v = $this->LabelHelper->valueByLabel("status", "xxx");
        $this->assertEquals($v, false, "illegal label");
    }

    /**
     * testLabelByValue method
     *
     * @return void
     */
    public function testLabelByValue() {
        $v = $this->LabelHelper->labelByValue("status", 0);
        $this->assertEquals($v ,"編集済み", "normal");

        $v = $this->LabelHelper->labelByValue("status", "xxx");
        $this->assertEquals($v , false, "illegal value");
    }

    public function testLabelByListAndValue() {
        $Label = TableRegistry::get('Labels');
        $query = $Label->find('list', [
            'keyField' => 'value',
            'valueField' => 'label',
        ]);
        $list = $query->toArray();
        $name = $this->LabelHelper->labelByListAndValue($list, 9);
        $this->assertEquals("不採用", $name);

        $name = $this->LabelHelper->labelByListAndValue($list, 33);
        $this->assertEmpty($name);

        return "";
    }

    /**
     * testSelectArray method
     *
     * @return void
     */
    public function testSelectArray() {
        $v = $this->LabelHelper->selectArray("status");

        $this->assertEquals($v, $this->_expectedArray, "select array");
    }

    /**
     * testCombo method
     *
     * @return void
     */
    public function testCombo() {
        $Form = $this->_setFormHelper();
        $Form->create("Label");
        $v = $this->LabelHelper->select("id", "status");
        $expected = <<<EOM
<select name="id">
<option value="0">編集済み</option>
<option value="1">採用</option>
<option value="9">不採用</option>
</select>
EOM;
        $expected = str_replace(["\r","\n"], '', $expected);
        $this->assertEquals($v, $expected, "select");

        $v = $this->LabelHelper->select("id", "status", array("empty" => "選択して下さい"));
        $expected = <<<EOM
<select name="id">
<option value="">選択して下さい</option>
<option value="0">編集済み</option>
<option value="1">採用</option>
<option value="9">不採用</option>
</select>
EOM;
        $expected = str_replace(["\r","\n"], '', $expected);
        $this->assertEquals($v, $expected, "select");
    }

    public function testCombo_WithName() {
        $Form = $this->_setFormHelper();
        $Form->create('Label');

        $v = $this->LabelHelper->select("Status.status", null, array("empty" => "選択して下さい"));
        $expected = <<<EOM
<select name="Status[status]">
<option value="">選択して下さい</option>
<option value="0">編集済み</option>
<option value="1">採用</option>
<option value="9">不採用</option>
</select>
EOM;
        $expected = str_replace(["\r","\n"], '', $expected);
        $this->assertEquals($v, $expected, "testCombo_WithName");

        $v = $this->LabelHelper->select("Status.id", "status", array("empty" => "選択して下さい"));
        $expected = <<<EOM
<select name="Status[id]">
<option value="">選択して下さい</option>
<option value="0">編集済み</option>
<option value="1">採用</option>
<option value="9">不採用</option>
</select>
EOM;
        $expected = str_replace(["\r","\n"], '', $expected);
        $this->assertEquals($v, $expected, "testCombo_WithName");
    }

    /* public testRadio() {{{ */
    /**
     * testRadio method
     *
     * @return void
     */
    public function testRadio() {
        $Form = $this->_setFormHelper();
        $Form->create("Label");
        $v = $this->LabelHelper->radio("id", "status");
        $expected = '<input type="hidden" name="id" value=""/>'
            . '<label for="id-0"><input type="radio" name="id" value="0" id="id-0">編集済み</label>'
            . '<label for="id-1"><input type="radio" name="id" value="1" id="id-1">採用</label>'
            . '<label for="id-9"><input type="radio" name="id" value="9" id="id-9">不採用</label>';
        $this->assertEquals($v, $expected, "radio");
    }
    // }}}
    /**
     * testHasValue method
     *
     * @return void
     */
    public function testHasValue() {
        $this->assertTrue($this->LabelHelper->hasValue("status", 1));
        $this->assertTrue($this->LabelHelper->hasValue("status", "1"));
        $this->assertFalse($this->LabelHelper->hasValue("status", 999999999999));
        $this->assertFalse($this->LabelHelper->hasValue("status", "999999999999"));
    }

    /* protected _setFormHelper() {{{ */
    /**
     * _setFormHelper
     *
     * @access protected
     * @return FormHelper
     */
    protected function _setFormHelper() {
        $Form = new FormHelper($this->View);
        $this->View->Form = $Form;
        return $Form;
    }
    // }}}

    /**
     * testGetInstance method
     * @return void
     */
    //public function testGetInstance() {
    //    $this->assertNotEmpty(LabelHelper::getInstance());
    //}
}
