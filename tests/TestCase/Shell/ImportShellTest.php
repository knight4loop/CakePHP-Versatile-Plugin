<?php
namespace Versatile\Test\TestCase\Shell;

use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Cake\Utility\Hash;
use Versatile\Shell\ImportShell;

/**
 * ImportShell Test Case
 *
 */
class ImportShellTest extends TestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = array('plugin.versatile.labels');

    /**
     * Test file directly
     *
     * @var array
     */
    protected $_datasDir = 'samples' . DS . 'tables';

    protected $_labelsArray = [
        [
            'id' => 1,
            'category' => 'gender',
            'label' => '男性',
            'value' => 1
        ],
        [
            'id' => 2,
            'category' => 'gender',
            'label' => '女性',
            'value' => 2
        ],
        [
            'id' => 3,
            'category' => 'is_deleted',
            'label' => '-',
            'value' => 0
        ],
        [
            'id' => 4,
            'category' => 'is_deleted',
            'label' => '削除済',
            'value' => 1
        ]
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->_path = Plugin::path('Versatile') . $this->_datasDir . DS;
        Configure::write('Versatile.ImportShell.path', $this->_path);
        $this->ImportShell = new ImportShell();
        $this->ImportShell->initialize();
        $this->Label = TableRegistry::get('Labels');
    }

    /**
     * testAll method
     *
     * @return void
     */
    public function testAll()
    {
        $this->ImportShell->runCommand(['all'], true, ['force'=>true]);
        $labels = $this->_findLabels();
        $this->assertEquals($labels, $this->_labelsArray);
    }

    /**
     * testAll method
     *
     * @return void
     */
    public function testAllExcel()
    {
        $this->ImportShell->runCommand(['all'], true, ['force'=>true, 'engine' => 'excel']);
        $labels = $this->_findLabels();
        $this->assertEquals($labels, $this->_labelsArray);
    }

    /**
     * testAll method
     *
     * @return void
     */
    public function testAllAll()
    {
        $this->ImportShell->runCommand(['all'], true, ['force'=>true]);
        $labels = $this->_findLabels();
        $this->assertEquals($labels, $this->_labelsArray);
    }

    /**
     * testNotAutoIncrement method
     *
     * @return void
     */
    public function testNotAutoIncrement()
    {
        $this->ImportShell->runCommand(['labels'], true, ['force'=>true, 'not-auto-increment' => true]);
        $labels = $this->_findLabels();
        $ids = Hash::extract($labels, '{n}.id');
        $labels = Hash::remove($labels, '{n}.id');
        $_ids = Hash::extract($this->_labelsArray, '{n}.id');
        $_labels = Hash::remove($this->_labelsArray, '{n}.id');
        $this->assertNotEquals($ids, $_ids);
        $this->assertEquals($labels, $_labels);
    }

    /**
     * testEnginYaml method
     *
     * @return void
     */
    public function testEnginYaml()
    {
        $this->ImportShell->runCommand(['labels'], true, ['force'=>true, 'engine' => 'yaml']);
        $labels = $this->_findLabels();
        $this->assertEquals($labels, $this->_labelsArray);
    }

    public function testYamlOtherFormat()
    {
        $path = Configure::read('Versatile.ImportShell.path') . 'labels-simple.yaml';
        $this->ImportShell->runCommand(['labels'], true, ['force'=>true, 'path' => $path, 'engine' => 'yaml']);
        $labels = $this->_findLabels();
        $this->assertEquals($labels, $this->_labelsArray);
    }

    /**
     * testEnginCsv method
     *
     * @return void
     */
    public function testEnginCsv()
    {
        $this->ImportShell->runCommand(['labels'], true, ['force'=>true, 'engine' => 'csv']);
        $labels = $this->_findLabels();
        $this->assertEquals($labels, $this->_labelsArray);
    }

    /**
     * testEnginExcel method
     *
     * @return void
     */
    public function testEnginExcel()
    {
        $this->ImportShell->runCommand(['labels'], true, ['force'=>true, 'engine' => 'excel']);
        $labels = $this->_findLabels();
        $this->assertEquals($labels, $this->_labelsArray);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ImportShell);

        parent::tearDown();
    }

    protected function _findLabels()
    {
        $labels = $this->Label->find('all', [
            'fields' => ['id', 'category', 'label', 'value'],
            "order" => ["id ASC"],
        ])->all();
        $arrLabels = [];
        foreach ($labels as $label) {
            $arrLabels[] = $label->toArray();
        }
        return $arrLabels;
    }
}
