<?php
namespace Versatile\Log;

use BadMethodCallException;
use Psr\Log\LogLevel;

trait LogTrait
{
    use \Cake\Log\LogTrait;

    protected $_logStartPrefix = '*** Start *** ';

    protected $_logEndPrefix = '*** End *** ';

    public function logStart($msg = null, $level = LogLevel::INFO, $context = [])
    {
        if ($msg === null) {
            $msg = $this->_logStartPrefix;
        }
        if (is_string($msg)) {
            $msg .= $this->_getTraceMessage(2);
        }
        $this->log($msg, $level, $context);
    }

    public function logEnd($msg = null, $level = LogLevel::INFO, $context = [])
    {
        if ($msg === null) {
            $msg = $this->_logEndPrefix;
        }
        if (is_string($msg)) {
            $msg .= $this->_getTraceMessage(2);
        }
        $this->log($msg, $level, $context);
    }

    protected function _getTraceMessage($depth = 1)
    {
        if (!is_numeric($depth)) {
            throw new BadMethodCallException('Depth is required to be integer.');
        }

        $debugbackTrace = debug_backtrace();
        if (!isset($debugbackTrace[$depth])) {
            return '';
        }

        $msg = '';
        $trace = $debugbackTrace[$depth];
        foreach (['class', 'type', 'function'] as $prop) {
            if (isset($trace[$prop])) {
                $msg .= $trace[$prop];
            }
        }
        if (isset($trace['function'])) {
            $msg .= '(';
            if (isset($trace['args'])) {
                $argType = [];
                foreach ($trace['args'] as $arg) {
                    $argType[] = gettype($arg);
                }
                $msg .= implode(', ', $argType);
            }
            $msg .= ')';
        }
        return $msg;
    }

}
