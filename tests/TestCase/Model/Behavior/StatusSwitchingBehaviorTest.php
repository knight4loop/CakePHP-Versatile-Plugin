<?php
namespace Versatile\Test\TestCase\Model\Behavior;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Cake\Utility\Hash;

/**
 * VersatileStatusSwitchingBehavior Test Case
 */
class StatusSwitchingBehaviorTest extends TestCase
{

/**
 * Fixtures associated with this test case
 *
 * @var array
 */
    public $fixtures = array(
        'plugin.versatile.StatusSwitching\Users',
        'plugin.versatile.StatusSwitching\UserDetails',
        'plugin.versatile.StatusSwitching\Articles',
        'plugin.versatile.StatusSwitching\ArticlesTags',
        'plugin.versatile.StatusSwitching\Tags',
        'core.comments',
    );

/**
 * setUp method
 *
 * @return void
 */
    public function setUp() {
        parent::setUp();
        $timestampConfig = [
            'events' => [
                'Model.beforeSave' => [
                    'created' => 'new',
                    'updated' => 'always',
                ],
                'Model.beforeDisable' => [
                    'disabled' => 'always'
                ]
            ]
        ];
        // This
        $this->Users = TableRegistry::get('Users');
        $this->Users->addBehavior('Versatile.StatusSwitching');
        $this->Users->addBehavior('Timestamp', $timestampConfig);
        // hasOne
        $this->UserDetails = TableRegistry::get('UserDetails');
        $this->UserDetails->addBehavior('Versatile.StatusSwitching');
        $this->UserDetails->addBehavior('Timestamp', $timestampConfig);
        // hasMany
        $this->Articles = TableRegistry::get('Articles');
        $this->Articles->addBehavior('Versatile.StatusSwitching');
        $this->Articles->addBehavior('Timestamp', $timestampConfig);
        // belongsToMany
        $this->Tags = TableRegistry::get('Tags');
        $this->Tags->addBehavior('Versatile.StatusSwitching');
        $this->Tags->addBehavior('Timestamp', $timestampConfig);
        TableRegistry::get('ArticlesTags')->addBehavior('Versatile.StatusSwitching');

        // Not had status field
        $this->Comments = TableRegistry::get('Comments');
        $this->Comments->addBehavior('Versatile.StatusSwitching');
        $this->Comments->addBehavior('Timestamp', $timestampConfig);

        $this->Users->addAssociations(array(
            'hasOne' => array(
                'UserDetails' => array(
                    'dependent' => true,
                )
            ),
            'hasMany' => array(
                'Articles' => array(
                    'dependent' => true,
                    'cascadeCallbacks' => true,
                ),
                'Comments' => array(
                    'dependent' => true,
                )
            ),
        ));

        $this->Tags->addAssociations(array(
            'belongsToMany' => array('Articles')
        ));

        $this->Articles->addAssociations(array(
            'hasMany' => array('Comments'),
            'belongsToMany' => array('Tags')
        ));
    }

/**
 * tearDown method
 *
 * @return void
 */
    public function tearDown() {
        $this->Users->removeBehavior('StatusSwitching');
        $this->Users->removeBehavior('Timestamp');
        $this->UserDetails->removeBehavior('StatusSwitching');
        $this->UserDetails->removeBehavior('Timestamp');
        $this->Articles->removeBehavior('StatusSwitching');
        $this->Articles->removeBehavior('Timestamp');
        $this->Tags->removeBehavior('StatusSwitching');
        $this->Tags->removeBehavior('Timestamp');
        TableRegistry::get('ArticlesTags')->removeBehavior('StatusSwitching');
        parent::tearDown();
    }

    /**
     * testBeforeFind method
     *
     * @return void
     */
    public function testBeforeFind() {
        $record = $this->Users
            ->find('all')
            ->contain([
                'UserDetails',
                'Articles',
                'Comments'
            ])
            ->toArray();
        $this->assertCount(2, $record);
        $this->assertCount(1, $record[0]['articles']);
        $assocConditions = $this->Users->association('UserDetails')->conditions();
        $this->assertEquals(true, empty($assocConditions));

        $query = $this->Users
            ->find('all')
            ->contain([
                'UserDetails',
                'Articles',
                'Comments'
            ])
            ->where([
                'Users.id' => 1
            ]);
        $this->assertEquals(true, preg_match('/is_disabled = /', $query->sql()));
        $this->assertCount(1, $query->toArray());

        $record = $this->Tags
            ->find('all')
            ->contain(['Articles'])
            ->toArray();
        $this->assertCount(2, $record[0]['articles']);

        $this->Users->runTimeStatusSwitching(false);
        $record = $this->Users
            ->find('all')
            ->contain('Articles')
            ->toArray();
        $this->assertCount(4, $record);
        $this->assertCount(1, $record[0]['articles']);

        $this->Users->runTimeStatusSwitching(false);
        $this->Users->Articles->runTimeStatusSwitching(false);
        $this->Users->UserDetails->runTimeStatusSwitching(false);
        $record = $this->Users
            ->find('all')
            ->contain(['Articles','UserDetails'])
            ->toArray();
        $this->assertCount(4, $record);
        $this->assertCount(2, $record[0]['articles']);
        $this->assertArrayHasKey('user_detail', $record[2]);

        $record = $this->Tags->ArticlesTags
            ->find('all')
            ->hydrate(false)
            ->toArray();
        $this->assertCount(4, $record);
    }

    /**
     * testDisable method
     *
     * @return void
     */
    public function testDisableHasOneAndHasMeny()
    {
        $entity = $this->Users->newEntity([
            'id' => 2,
        ]);
        $res = $this->Users->disable($entity, [
            'associated' => false
        ]);
        $record = $this->Users
            ->find('all')
            ->toArray();
        $this->assertCount(1, $record);

        $this->Users->runTimeStatusSwitching(false);
        $this->UserDetails->runTimeStatusSwitching(false);
        $this->Articles->runTimeStatusSwitching(false);

        $entity = $this->Users->newEntity([
            'id' => 1,
        ]);
        $this->Users->disable($entity);
        $record = $this->Users
            ->find('all')
            ->contain(['Articles','UserDetails','Comments'])
            ->hydrate(false)
            ->toArray();
        $record = Hash::combine($record, '{n}.id', '{n}');
        $first = $record[1];
        $this->assertEquals(1, $first['is_disabled']);
        $this->assertNotEmpty($first['disabled']);
        $this->assertEquals(1, $first['user_detail']['is_disabled']);
        $this->assertNotEmpty($first['user_detail']['disabled']);
        $this->assertEquals(1, $first['articles'][0]['is_disabled']);
        $this->assertEquals(1, $first['articles'][1]['is_disabled']);
        $this->assertNotEmpty($first['articles'][0]['disabled']);
        $this->assertNotEmpty($first['articles'][1]['disabled']);

        $third = $record[3];
        $this->assertNotEquals(1, $third['articles'][0]['is_disabled']);
    }

    public function testEnableHasOneAndHasMeny()
    {
        $entity = $this->Users->newEntity([
            'id' => 3,
        ]);
        $this->Users->enable($entity);
        $this->Users->runTimeStatusSwitching(false);
        $this->UserDetails->runTimeStatusSwitching(false);
        $this->Articles->runTimeStatusSwitching(false);
        $record = $this->Users
            ->find('all')
            ->contain(['Articles','UserDetails','Comments'])
            ->hydrate(false)
            ->toArray();
        $record = Hash::combine($record, '{n}.id', '{n}');
        $first = $record[3];
        $this->assertEquals(0, $first['is_disabled']);
        $this->assertEquals(0, $first['user_detail']['is_disabled']);
        $this->assertEquals(0, $first['articles'][0]['is_disabled']);

        $third = $record[3];
        $this->assertNotEquals(1, $third['articles'][0]['is_disabled']);
    }

    /**
     * testDisable method
     *
     * @return void
     */
    public function testDisableBelongsToMany()
    {
        $entity = $this->Tags->newEntity([
            'id' => 1,
        ]);
        $this->Tags->disable($entity);

        $record = $this->Tags
            ->find('all')
            ->contain(['Articles'])
            ->hydrate(false)
            ->toArray();
        $this->assertCount(1, $record);

        $record = $this->Tags->ArticlesTags
            ->find('all')
            ->hydrate(false)
            ->toArray();
        $this->assertCount(1, $record);
    }

    /**
     * testDisable method
     *
     * @return void
     */
    public function testEnableBelongsToMany()
    {
        $entity = $this->Tags->newEntity([
            'id' => 3,
        ]);
        $this->Tags->enable($entity);

        $record = $this->Tags
            ->find('all')
            ->contain(['Articles'])
            ->hydrate(false)
            ->toArray();
        $this->assertCount(3, $record);

        $record = $this->Tags->ArticlesTags
            ->find('all')
            ->hydrate(false)
            ->toArray();
        $this->assertCount(5, $record);
    }

    /**
     * testDisableAll method
     *
     * @return void
     */
    public function testDisableAll()
    {
        $this->markTestIncomplete('testDisableAll not implemented.');
    }

    /**
     * testStatusSwitching method
     *
     * @return void
     */
    public function testEnableAll()
    {
        $this->markTestIncomplete('testEnableAll not implemented.');
    }

}
