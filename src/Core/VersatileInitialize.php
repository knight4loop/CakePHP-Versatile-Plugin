<?php
/**
 * Versatile のプラグインを使用するのに必要な設定値を初期化するクラスです。
 *
 */
namespace Versatile\Core;

use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Core\Plugin;
use Cake\Utility\Hash;

class VersatileInitialize
{
    protected static $_initialized = [];

    public static function all($force = false)
    {
        static::config($force);
    }

    public static function config ($force = false)
    {
        if (!$force && static::_isInitialized(__METHOD__)) {
            return;
        }
        $path = Plugin::path('Versatile');

        $appConfig = (array)Configure::read('Versatile');

        if (!Configure::configured('Versatile.default')) {
            Configure::config('Versatile.default', new PhpConfig($path . 'config' . DS));
        }

        Configure::load('default', 'Versatile.default', true);
        $defaultConfig = (array)Configure::read('Versatile');

        $config = Hash::merge($defaultConfig, $appConfig);

        Configure::write('Versatile', $config);

        static::_initialize(__METHOD__);
    }

    protected static function _isInitialized($key)
    {
        return isset(static::$_initialized[$key]) && static::$_initialized[$key];
    }

    protected static function _initialize($key)
    {
        static::$_initialized[$key] = true;
    }
}
