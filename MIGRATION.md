CakePHP2系 -> CakePHP3系 移行機能一覧
====

## Console/Command
- **[MODIFIED]** BaseSeedShell.php : Admin User などファーストユーザ作成用ベースクラス -> src/Shell->UserSeedShell.php
- **[MODIFIED]** CacheClearShell.php : CakePHP のキャッシュクリア -> src/Shell/CacheClearShell.php
- **[MODIFIED]** GenerateConstantsShell.php : 定数ファイルを作成する -> src/Shell/GenarateShell.php
- GenerateSecuritySeedShell.php : Security.salt, Security.cipherSeed, Security.key を作成する
- GenerateValidationMessageShell.php : バリデーションメッセージファイルを作成する
- **[MODIFIED]** InitialImportShell.php : Label や マスターデータをデータベースに登録する -> src/Shell/ImportShell.php
- TestDataExportShell.php : テストデータの xls ファイルを読み込みデータベースに登録する
- TestDataImportShell.php : テストデータの xls ファイルを作成する

## Controller/Component
- PrgUtilComponent.php : 存在しない日付でも日付文字列（2000/01/00 など）を作成するメソッドを提供するコンポーネント
- ProtocolHandlerComponent.php : SSL 表示強制コンポーネント
- SSLLoadBalanceDetectComponent.php : リバースプロキシで CakePHP を動かすためのコンポーネント

## Lib
- Panel/GitInfoPanel.php : DebugKit で 現在の Git 情報を出すためのクラス。
- ArrayUtil.php : 配列系
- BankUtil.php : 銀行口座系（と言っても銀行口座番号を返すメソッドがあるだけ）
- **[MODIFIED]** CSVUtil.php : CSV 読み込み、書き込み用 -> src/Utility/ExcelUtil.php
- CacheUtil.php : キャッシュ系（CakePHP::Cache を便利にしているがなくてもよい）
- ConfigManager.php : Config ファイルを読み込むためのクラス
- CreditUtil.php : クレジットカード系
- CryptUtil.php : Crypt 方式（シーザー暗号）による文字列を生成・解読する。その他キーの作成など。
- Decimal.php : 数値系
- **[MODIFIED]** EnvUtil.php : 環境変数により読み込むファイルを変更するためのクラス -> src/Core/Environment.php
- **[MODIFIED]** ExcelUtil.php : Excel を読み込むためのクラス -> src/Utility/ExcelUtil.php
- FileUtil.php : ファイル操作系
- HttpUtil.php : HttpSoket 操作系
- LogUtil.php : ログ系。（主にSQLのログを吐き出すためのロジックっぽいけど使い方が不明。）
- MobileUtil.php : モバイル端末判断。日本国内用。
- PasswordGenerator.php : パスワード発行を行うクラス。
- ReverseFileReader.php : ファイルを先頭から込みこむためのクラス。
- SSLBalancerUtil.php : リバースプロキシ判断用。
- StringUtil.php : 文字列用。
- VersatileConfigUtil.php : Versatile で生成する Config ファイルの判断をしている。(削除予定)

## Model/Behavior
- OptimisticLockBehavior.php : 楽観的排他ロック機能を提供します。（とReadMeに書いてある）
- TransactionBehavior.php : begin, commit, rollback, savePoint を $this->Model の記述で操作できるようにします。
- VersatileAutoConvertBehavior.php : フィールドを保存する前に値の自動変換機能を提供します。（全角 → 半角）
- VersatileConditionBehavior.php : Search.Prg 用の条件生成メソッドを提供します。
- VersatileCryptFieldBehavior.php : 特定のフィールドを暗号化する機能を提供します。
- VersatileDisableStatusBehavior.php : 論理削除機能を提供します。
- VersatileFindBehavior.php : 検索用の基本的なメソッドを提供します。
- VersatileJoinHelperBehavior.php : join に必要な Array を返却します。定義はモデルに記述されている `hasOne`, `belongsTo`, `hasMany` を参照して作成します。
- VersatileMessageFormatBehavior.php : GenerateValidationMessageShell で作成したバリデーションメッセージをモデルに登録します。
- VersatileValidationsBehavior.php : いくつかのバリデーション用メソッドを提供します。
- VersatileValidationsSwitcherBehavior.php : バリデーションをスイッチできるようにします。

## Model/Datasource/Database
- MysqlLog.php : MySQL で実行時にログを出力するためのWrapper。
- PostgresLog.php : PostgreSQL で実行時にログを出力するためのWrapper。
- SqliteLog.php : SqlireSQL で実行時にログを出力するためのWrapper。
- Sqlserver.php : Sqlserver DBO で実行時にログを出力するためのWrapper。

## View
- MobileTwigView.php : フィーチャーフォン、スマートフォン向けのテンプレート切り替え機能を提供します。
- VersatileView : 通常のViewクラスにない機能を提供します。

## View/Elements
- DebugKit/gitinfo_panel.ctp : DebugKit で Git 情報を出すためのテンプレート。

## View/Helper
- **[DELETED]** ArrayHelper.php : 配列操作用ヘルパー。（TwigView用）
- CreditCardHelper.php : クレジットカード用ヘルパー。
- ImageHelper.php : 画像をリサイズします。 gd 拡張が必要です。
- **[MODIFIED]** LabelHelper.php : Label テーブルを操作を行います。-> src/View/Helper/LabelHelper.php
- **[DELETED]** SetHelper.php : CakePHP::Set用ヘルパー。（TwigView用）
- SimpleFormHelper.php : フォームをシンプルなものにするためのヘルパー。
- SimpleXformjpHelper.php : プラグイン Xform の拡張用ヘルパー。
