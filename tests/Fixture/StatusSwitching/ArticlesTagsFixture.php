<?php
namespace Versatile\Test\Fixture\StatusSwitching;

use Cake\TestSuite\Fixture\TestFixture;

class ArticlesTagsFixture extends TestFixture
{
    /**
     * fields property
     *
     * @var array
     */
    public $fields = [
        'article_id' => ['type' => 'integer', 'null' => false],
        'tag_id' => ['type' => 'integer', 'null' => false],
        'is_disabled' => ['type' => 'integer', 'null' => true, 'default' => 0],
        'disabled' => ['type'=>'datetime', 'null' => true],
        '_constraints' => ['primary' => ['type' => 'primary', 'columns' => ['article_id', 'tag_id']]]
        //'indexes' => ['UNIQUE_TAG2' => ['column' => ['article_id', 'tag_id'], 'unique' => 1]]
    ];

    /**
     * records property
     *
     * @var array
     */
    public $records = [
        ['article_id' => 1, 'tag_id' => 1],
        ['article_id' => 1, 'tag_id' => 2],
        ['article_id' => 2, 'tag_id' => 1],
        ['article_id' => 2, 'tag_id' => 3, 'is_disabled' => 1, 'disabled' => '2007-03-18 12:28:31'],
        ['article_id' => 3, 'tag_id' => 1]
    ];
}
