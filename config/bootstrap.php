<?php
use Versatile\Core\VersatileInitialize;
use Cake\Core\Configure;

VersatileInitialize::all();

$constantsFilePath = Configure::read('Versatile.Constants.file');
if ($constantsFilePath && \file_exists($constantsFilePath)) {
    require $constantsFilePath;
}
