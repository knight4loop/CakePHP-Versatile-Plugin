<?php
/**
 * PasswordGenerator Test Case
 *
 */
namespace Versatile\Test\TestCase\Utility;

use Cake\TestSuite\TestCase;
use Versatile\Utility\PasswordGenerator;

class PasswordGeneratorTest extends TestCase
{
    public function testDefault()
    {
        $pass = PasswordGenerator::create();
        $this->assertEquals(8, strlen($pass));
        $this->assertRegExp('!^[a-zA-Z0-9]*$!', $pass);
    }

    public function testTypes()
    {
        $pass = PasswordGenerator::create(null, 'small');
        $this->assertEquals(8, strlen($pass));
        $this->assertRegExp('!^[a-z]*$!', $pass);

        $pass = PasswordGenerator::create(null, 'large');
        $this->assertEquals(8, strlen($pass));
        $this->assertRegExp('!^[a-zA-Z]*$!', $pass);

        $pass = PasswordGenerator::create(null, 'number');
        $this->assertEquals(8, strlen($pass));
        $this->assertRegExp('!^[0-9]*$!', $pass);

        $pass = PasswordGenerator::create(null, 'sign');
        $this->assertEquals(8, strlen($pass));
        $this->assertRegExp('/^[!#$%&*+@?]*$/', $pass);

        $pass = PasswordGenerator::create(null, ['alphabet']);
        $this->assertEquals(8, strlen($pass));
        $this->assertRegExp('!^[a-zA-Z]*$!', $pass);

        $pass = PasswordGenerator::create(null, ['alphabet', 'number', 'sign']);
        $this->assertEquals(8, strlen($pass));
        $this->assertRegExp('/^[a-zA-Z0-9!#$%&*+@?]*$/', $pass);
    }

    public function testInvaildSetting()
    {

        $pass = PasswordGenerator::create(null, 'test');
        $this->assertEquals(8, strlen($pass));
        $this->assertRegExp('!^[a-zA-Z0-9]*$!', $pass);
    }

    public function testConfig()
    {
        $pass = PasswordGenerator::config([
            'length' => 9,
            'types' => 'number'
        ]);
        $pass = PasswordGenerator::create();
        $this->assertEquals(9, strlen($pass));
        $this->assertRegExp('!^[0-9]*$!', $pass);

        $pass = PasswordGenerator::config(7, 'alphabet');
        $pass = PasswordGenerator::create();
        $this->assertEquals(7, strlen($pass));
        $this->assertRegExp('!^[a-zA-Z0-9]*$!', $pass);
    }
}
