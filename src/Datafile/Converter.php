<?php
namespace Versatile\Datafile;

use Cake\Core\Configure;
use Cake\Exception\Exception;
use Cake\Utility\Inflector;

class Converter
{
    static protected $_namespaces = [];

    public static function add($path, $pluginName = null)
    {
        if ($pluginName === null) {
            $className = static::_appNamespace($path);
        } else {
            if (strpos($path, '\\') !== 0) {
                $path = '\\' . $path;
            }
            $className = '\\' . $pluginName . static::_className($path);
        }
        if (!in_array($className, static::$_namespaces)) {
            static::$_namespaces[] = $className;
        }
    }

    public static function convert(&$data, $name)
    {
        $className = static::_converterName($name);
        if (!$className) {
            throw new Exception(sprintf('Not Found Class : %s', $className));

        }
        return $className::exec($data);
    }

    protected static function _appNamespace($name)
    {
        if ($name && strpos($name, '\\') !== 0) {
            $name = '\\' . $name;
        }

        $appNamespace = Configure::read('App.namespace');
        if ($appNamespace) {
            $appNamespace = '\\' . $appNamespace;
        }

        return static::_className($appNamespace . $name);
    }

    protected static function _className($name)
    {
        if (substr($name, -1) === '\\') {
            $name = substr($name, 0, strlen($name) - 1);
        }
        return $name;
    }

    protected static function _converterName($name)
    {
        $name = Inflector::classify($name);

        $default = '\\Datafile\\Converter';

        $namespaces = [static::_appNamespace($default)];

        $namespaces = array_merge($namespaces, static::$_namespaces);

        array_push($namespaces, static::_className('\\Versatile' . $default));

        $className = null;
        $suffixs = ['', 'Converter'];
        foreach ($namespaces as $idx => $namespace) {
            foreach($suffixs as $suffix) {
                $className = $namespace . '\\' . $name . $suffix;
                if (class_exists($className)) {
                    break 2;
                }
            }
        }
        return $className;
    }
}
