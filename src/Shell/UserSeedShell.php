<?php
namespace Versatile\Shell;

use Cake\Core\Configure;
use Cake\Utility\Hash;
use Versatile\Console\VersatileShell;

class UserSeedShell extends VersatileShell
{
    protected $_configName = 'Versatile.UserSeedShell';

    public function main()
    {
        $configName = $this->_configName;
        if ($this->args && $this->args[0]) {
            $configName .= '.' . $this->args[0];
        }
        $config = Configure::read($configName);
        if (!is_numeric(key($config))) {
            $config = [$config];
        }
        foreach ($config as $c) {
            $this->_process($c);
        }
    }

    protected function _process($config = null)
    {
        $modelName = Hash::get($config, 'userModel');

        if (empty($modelName)) {
            $this->error(
                'Configure is incorrect.',
                'Check your "'. $this->_configName .'" in Configure settings.'
            );
        }

        $Model = $this->loadModel($modelName);

        $fields = $this->_getFields($Model, Hash::get($config, 'fields'));

        $data = [];
        $len = 0;
        foreach ($fields as $f) {
            $val = $this->in("Enter [$f] :");
            $data[$f] = $val;
            $_len = strlen($f);
            if ($len < $_len) {
                $len = $_len;
            }
        }
        $this->hr();
        $this->out($modelName);
        foreach ($data as $key => $val) {
            $this->out(sprintf("    %-{$len}s : %s", $key, $val));
        }
        $select = strtolower($this->in("Create ok?", ['y', 'n'], 'n'));
        if ($select !== 'y') {
            return;
        }
        $data = $this->_convertData($data);
        $newEntity = $Model->save($Model->newEntity($data));
        if ($newEntity) {
            $this->info("Success.");
        } else {
            $this->err("Faild.\n"
                . " Please check folloing messages\n"
                . $this->_getValidationMessages($Model));
        }
        return;
    }

    protected function _convertData ($data = [])
    {
        return $data;
    }

    protected function _getFields($Model, $fields = [])
    {
        if (empty($fields)) {
            $primaryKey = $Model->primaryKey();
            $exclude = ['updated', 'modified', 'created'];
            $fields = $Model->schema()->columns();
            foreach ($fields as $key => $field) {
                if (in_array($field, $exclude)) {
                    unset($fields[$key]);
                    continue;
                }
                if ($field === $primaryKey) {
                    $primaryKeyColumn = $Model->schema()->column($primaryKey);
                    if ($primaryKeyColumn['autoIncrement']) {
                        unset($fields[$key]);
                        continue;
                    }
                }
            }
        } elseif (is_string($fields)) {
            $fields = [$fields];
        }
        return $fields;
    }

    protected function _getValidationMessages($Model)
    {
        $m = array();
        foreach ($Model->validationErrors as $key => $msgs) {
            $m[] = sprintf("%s ->\n  %s", $key, implode("\n  ", $msgs));
        }
        return implode("\n", $m);
    }

}
