<?php
/**
 * AdminUsersFixture
 *
 */
namespace Versatile\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

class AdminUsersFixture extends TestFixture
{

/**
 * Fields
 *
 * @var array
 */
    public $fields = [
        'id' => ['type' => 'integer'],
        'name' => ['type' => 'string', 'null' => false],
        'email' => ['type' => 'string', 'null' => false],
        'password' => ['type' => 'string', 'null' => false],
        'role_type' => ['type' => 'integer', 'null' => false],
        'created' => ['type' => 'datetime', 'null' => false],
        'modified' => ['type' => 'datetime', 'null' => false],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id']]
        ]
    ];

/**
 * Records
 *
 * @var array
 */
    public $records = [
        ['name' => 'super', 'email' => 'super@admin_users.jp', 'password' => 'password1', 'role_type' => 1, 'created' => '2016/04/11 16:56:00', 'modified' => '2016/04/11 16:56:00'],
        ['name' => 'corporation', 'email' => 'corporation@admin_users.jp', 'password2' => 'password4', 'role_type' => 2, 'created' => '2016/04/11 16:56:00', 'modified' => '2016/04/11 16:56:00'],
        ['name' => 'area', 'email' => 'area@admin_users.jp', 'password' => 'password3', 'role_type' => 3, 'created' => '2016/04/11 16:56:00', 'modified' => '2016/04/11 16:56:00'],
        ['name' => 'site', 'email' => 'site@admin_users.jp', 'password' => 'password4', 'role_type' => 4, 'created' => '2016/04/11 16:56:00', 'modified' => '2016/04/11 16:56:00'],
    ];

}
