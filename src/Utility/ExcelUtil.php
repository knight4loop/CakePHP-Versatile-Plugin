<?php
namespace Versatile\Utility;

/**
 * ExcelContext
 *
 * @package
 * @version $id$
 * @copyright
 * @license PHP Version 3.0 {@link http://www.php.net/license/3_0.txt}
 */
class ExcelContext {
    /**
     * シートの最大文字長
     * @var integer
     */
    const MAX_SHEET_NAME_LEN = 31;
    /**
     * @var PHPExcel_Sheet
     * @access public
     */
    var $xl;
    /**
     * @var array
     * @access public
     */
    var $sheets;
    /**
     * @var string
     * @access public
     */
    var $sheetName;
    /**
     * @var array
     * @access public
     */
    var $sheetLines;

    var $_withHash = true;

    var $_skipHeader = false;

    var $_ignoreFirstRowBlank = false;

    var $_dirty = true;

    function __construct($fpath = null, $type = null) {
        if (isset($fpath)) {
            $this->open($fpath, $type);
        }
    }


    /* public open($fpath, $type = null) {{{ */
    /**
     * open
     *
     * @param mixed $fpath
     * @param bool $type
     * @access public
     * @return void
     */
    function open($fpath, $type = null) {
        if (!isset($type)) {
            $ext = substr(strrchr($fpath, '.'), 1);
            if ($ext == "xlsx") {
                $type = "Excel2007";
            } elseif ($ext == "xml") {
                $type = "Excel2003XML";
            } elseif ($ext == "ods") {
                $type = "OOCalc";
            } else {
                $type = "Excel5";
            }
        }
        // autoload
        $reader = \PHPExcel_IOFactory::createReader($type);
        $this->xl = $reader->load($fpath);
        $this->sheets = $this->xl->getSheetNames();
    }
    // }}}

    /**
     * シートが存在するかどうかを判定します。
     * @param string $name
     * @return boolean
     */
    function hasSheet($name) {
        return array_search($name, $this->xl->getSheetNames()) !== false;
    }

    /**
     * 読み込む対象を隣のシートに変更します。
     * @return boolean
     */
    function next() {
        $sheet_name = array_shift($this->sheets);
        if (empty($sheet_name)) {
            return false;
        }
        $this->sheetName = $sheet_name;
        $this->_dirty = true;
        return true;
    }

    /**
     * 読み込むシート名を設定します。
     * @param string $name
     * @return ExcelContext
     */
    function sheet($name) {
        if (strlen($name) > self::MAX_SHEET_NAME_LEN) {
            $name = substr($name, 0, self::MAX_SHEET_NAME_LEN);
        }
        $this->sheetName = $name;
        $this->_dirty = true;
        return $this;
    }

    /**
     * データの読み込みをハッシュで行います。(キーは先頭行)
     * @return ExcelContext
     */
    function byHash() {
        $this->_withHash = true;
        $this->_dirty = true;
        $this->_ignoreFirstRowBlank = false;
        return $this;
    }

    /**
     * データの読み込みを配列で行います。
     * @return ExcelContext
     */
    function byArray() {
        $this->_withHash = false;
        $this->_dirty = true;
        $this->_ignoreFirstRowBlank = false;
        return $this;
    }

    /**
     * データの読み込みを配列で行います。
     * <p>
     * 先頭行にブランクセルが存在する場合、２行目以降も
     * ブランクセルの前の列データまでしか読み込まないよう設定します。
     * </p>
     * @return ExcelContext
     */
    function byFixedArray() {
        $this->_withHash = false;
        $this->_dirty = true;
        $this->_ignoreFirstRowBlank = true;
        return $this;
    }

    /**
     * データの読み込みをハッシュで行います。(キーは先頭行)
     * <p>
     * 先頭行にブランクセルが存在する場合、２行目以降も
     * ブランクセルの前の列データまでしか読み込まないよう設定します。
     * </p>
     * @return ExcelContext
     */
    function byFixedHash() {
        $this->_withHash = true;
        $this->_dirty = true;
        $this->_ignoreFirstRowBlank = true;
        return $this;
    }

    /**
     * Excelシートを読み込みます。
     * @return array
     */
    function read() {
        if ($this->_dirty) {
            $datas = $this->readSheet($this->sheetName, $this->_withHash, $this->_ignoreFirstRowBlank);
            $this->sheetLines = $datas;
            $this->_dirty = false;
        }
        return $this->sheetLines;
    }

    /**
     * Excelシートを読み込みます。
     * @return array
     */
    function get() {
        return $this->read();
    }

    /**
     * １行内のカラムを特定のセパレータでつなげた行の配列で返却します。
     * @param string $sep
     * @param boolean $skip_header
     * @param boolean $ignore_first_line_blank
     * @return multitype:string
     */
    function lines($sep, $skip_header = false, $ignore_first_line_blank = false) {
        $lines = $this->readSheet($this->sheetName, false, $ignore_first_line_blank);
        $datas = array();
        if ($skip_header) {
            array_shift($lines);
        }
        foreach ($lines as $line) {
            $datas[] = implode($sep, $line);
        }
        return $datas;
    }

    /* public readSheet() {{{ */
    /**
     * シートからデータを読み出します。
     *
     * @param string $sheet_name シート名
     * @param boolean $with_hash
     * @access public
     * @return mixed array
     */
    function readSheet($sheet_name, $with_hash = true, $ignore_first_line_blank = false) {
        $xl = $this->xl;

        if (empty($sheet_name)) {
            return false;
        }
        $sheet = $xl->getSheetByName($sheet_name);
        // $sheet = $xl->getActiveSheet();

        $max_row = $sheet->getHighestRow();
        $max_col = $sheet->getHighestColumn();
        $max_col_index = \PHPExcel_Cell::columnIndexFromString($max_col);

        $header = null;
        $datas = array();
        for ($i = 1; $i <= $max_row; $i++) {
            $line = array();
            for ($j = 0; $j < $max_col_index; $j++) {
                $colstr = \PHPExcel_Cell::stringFromColumnIndex($j);
                $cell = $sheet->getCell($colstr.$i);
                // $value = $cell->getValue();
                // $value = $cell->getCalculatedValue();
                $value = $cell->getFormattedValue();
                if ($i == 1 && $ignore_first_line_blank && $value == "") {
                    $max_col_index = $j;
                    break;
                }
                $line[] = $value;
            }
            if ($with_hash) {
                if (empty($header)) {
                    $header = $line;
                } else {
                    $datas[] = array_combine($header, $line);
                }
            } else {
                $datas[] = $line;
            }
        }
        return $datas;
    }
    // }}}

    function sheetContextWithArray() {
        return $this->sheetContext($sheet_name, false, false);
    }

    function sheetContextWithFixedArray() {
        return $this->sheetContext($sheet_name, false, true);
    }

    function sheetContextWithHash() {
        return $this->sheetContext($sheet_name, true, false);
    }

    function sheetContextWithFixedHash() {
        return $this->sheetContext($sheet_name, true, true);
    }

    function sheetContext($sheet_name, $with_hash = true, $ignore_first_line_blank = false) {
        $xl = $this->xl;

        if (empty($sheet_name)) {
            return false;
        }
        $sheet = $xl->getSheetByName($sheet_name);

        return new ExcelSheetContext($sheet, $with_hash, $ignore_first_line_blank);
    }

}

class ExcelSheetContext {
    var $sheet = null;
    var $with_hash = true;
    var $ignore_first_line_blank = false;
    var $max_row = 0;
    var $max_col = 0;
    var $max_col_index = 0;
    var $row = 1;
    var $header = array();

    function __construct($sh, $with_hash = true, $ignore_first_line_blank = false) {
        $this->sheet = $sh;

        if (empty($sh)) {
            throw new RuntimeException("object not found");
        }

        $this->with_hash = $with_hash;
        $this->ignore_first_line_blank = $ignore_first_line_blank;
        $this->max_row = $sheet->getHighestRow();
        $this->max_col = $sheet->getHighestColumn();
        $this->max_col_index = \PHPExcel_Cell::columnIndexFromString($max_col);

        $this->row = 1;

        if ($this->with_hash) {
            $this->header = $this->_readLine();
        }

    }

    function readAll() {
        $lines = array();
        while (!$this->isEof()) {
            $lines[] = $this->readLine();
        }
        return $lines;
    }

    function read() {
        return $this->readLine();
    }

    function readLine() {
        $line = $this->_readLine();
        if ($this->with_hash) {
            $line = array_combine($this->header, $line);
        }
        return $line;
    }

    function _readLine() {
        $i = $this->row;
        $this->row++;

        $line = array();
        for ($j = 0; $j < $this->max_col_index; $j++) {
            $colstr = \PHPExcel_Cell::stringFromColumnIndex($j);
            $cell = $this->sheet->getCell($colstr.$i);
            // $value = $cell->getValue();
            // $value = $cell->getCalculatedValue();
            $value = $cell->getFormattedValue();
            if ($i == 1 && $this->ignore_first_line_blank && $value == "") {
                $this->max_col_index = $j;
                break;
            }
            $line[] = $value;
        }
        return $line;
    }

    function isEof() {
        return $this->row > $this->max_row;
    }
}

/**
 * ExcelUtil
 *
 * @package
 * @version $id$
 * @copyright
 * @license PHP Version 3.0 {@link http://www.php.net/license/3_0.txt}
 */
class ExcelUtil {

    static function autoload() {
        // do nothing
    }
    /* public open($fpath, $type=null) {{{ */
    /**
     * open
     *
     * @param mixed $fpath
     * @param bool $type
     * @static
     * @access public
     * @return void
     */
    static function open($fpath, $type=null) {
        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
        return new ExcelContext($fpath, $type);
    }
    // }}}

    /* public openNicely($fpath) {{{ */
    /**
     * 適当にファイルがあったら開きます。(主にテスト用)
     *
     * @param mixed $fpath
     * @static
     * @access public
     * @return void
     */
    static function openNicely($fpath) {
        $pos = strrpos($fpath, ".");
        $prefix = substr($fpath, 0, $pos);
        foreach (array(".xlsx", ".xls") as $ext) {
            $xlpath = $prefix . $ext;
            if (file_exists($xlpath)) {
                return self::open($xlpath);
            }
        }
        throw new InvalidArgumentException("file not found : $fpath");
    }
    // }}}
}

