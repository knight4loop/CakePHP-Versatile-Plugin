<?php
/**
 * OptimisticLockFixture
 *
 */
namespace Versatile\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

class OptimisticLocksFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    public $fields = array(
        'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
        'content' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
        'version' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 40, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
        'locked' => ['type' => 'datetime', 'null' => true],
        'created' => ['type' => 'datetime', 'null' => false],
        'modified' => ['type' => 'datetime', 'null' => false],
        '_options' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB'),
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id']]
        ]
    );

    /**
     * Records
     *
     * @var array
     */
    public $records = array(
        array(
            'id' => '1',
            'content' => 'record1',
            'version' => ''
        ),
        array(
            'id' => '2',
            'content' => 'record2',
            'version' => '5302cfcf-ea10-43b3-9548-024b8c2d5341'
        ),
        array(
            'id' => '3',
            'content' => 'record3',
            'version' => '5302cfcf-ea10-43b3-9548-024b8c2d5341'
        ),
        array(
            'id' => '4',
            'content' => 'record4',
            'version' => '5302cfcf-ea10-43b3-9548-024b8c2d5341'
        ),
    );
}
