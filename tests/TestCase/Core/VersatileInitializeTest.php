<?php
namespace Versatile\Test\TestCase\Core;

use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Core\Plugin;
use Cake\TestSuite\TestCase;
use Versatile\Core\VersatileInitialize;

class VersatileInitializeTest extends TestCase
{
    public function testConfig()
    {
        $PhpConfig = new PhpConfig(Plugin::path('Versatile') . 'config' . DS);
        $configForAssert = $PhpConfig->read('default')['Versatile'];

        VersatileInitialize::config();
        $config = Configure::read('Versatile');

        $this->assertEquals($config, $configForAssert);
    }
}

