<?php
namespace Versatile\Datafile\Engine;

use Versatile\Utility\CSVUtil;
use Versatile\Datafile\Engine\BaseDatafileEngine;
use Versatile\Datafile\TextFileDatafileTrait;

class CsvDatafileEngine extends BaseDatafileEngine
{
    use TextFileDatafileTrait;

    protected $_csvUtil = null;

    public function __construct() {
        $this->_csvUtil = new CSVUtil();
    }

    public function read($sheet = null)
    {
        $path = $this->_getTextFilePath($sheet);

        $this->path($path);

        $csv = CSVUtil::getReader($path);

        $lines = $csv->readAll();
        $header = array_shift($lines);
        foreach ($header as $idx => $key) {
            $header[$idx] = trim($key);
        }

        $contents = [];
        foreach ($lines as $line) {
            $contents[] = array_combine($header, $line);
        }

        return $contents;
    }
}
