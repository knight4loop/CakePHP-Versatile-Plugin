<?php
/**
 * OptimisticLockBehavior Test Case
 *
 */
namespace Versatile\Test\TestCase\Model\Behavior;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Versatile\Model\Behavior\Exception\OptimisticLockException;

class OptimisticLockBehaviorTest extends TestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = array(
        'plugin.versatile.optimistic_locks',
    );

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->OptimisticLocks = TableRegistry::get('OptimisticLocks');
        $this->OptimisticLocks->addBehavior('Versatile.OptimisticLock');
        $this->OptimisticLockBehavior = $this->OptimisticLocks->behaviors()->get('OptimisticLock');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        $this->OptimisticLocks->removeBehavior('OptimisticLock');
        unset($this->OptimisticLockBehavior);
        unset($this->OptimisticLocks);
        parent::tearDown();
    }


    /**
     * testSaveMessage method
     *
     * @return void
     */
    public function testSingleSaveMessage()
    {
        $this->_enableSingleConfig();
        $this->_processSave('message');
    }

    /**
     * testSaveException method
     * @throw
     * @return void
     */
    public function testSingleSaveException()
    {
        $this->_enableSingleConfig();
        $this->_processSave('exception');
    }

    public function testSingleSaveMessageWhenMultiLock()
    {
        $this->_processSave('message', true);
    }

    public function testSingleSaveExceptionWhenMultiLock()
    {
        $this->_processSave('exception', true);
    }

    /**
     * testMultiSaveMessageWhenMultiLock
     *
     * @access public
     * @return void
     */
    public function testMultiSaveMessageWhenMultiLock()
    {
        $this->_processMultiSaveWhenMultiLock('message');
    }

    /**
     * testMultiLockMessageWhenMultiLock
     *
     * @access public
     * @return void
     */
    public function testMultiLockMessageWhenMultiLock()
    {
        $this->_processMultiLockWhenMultiLock('message');
    }

    /**
     * testMultiSaveExceptionWhenMultiLock
     *
     * @access public
     * @return void
     */
    public function testMultiSaveExceptionWhenMultiLock()
    {
        $this->_processMultiSaveWhenMultiLock('exception');
    }

    public function testMultiLockExceptionWhenMultiLock()
    {
        $this->_processMultiLockWhenMultiLock('exception');
    }

    public function testOffAuto()
    {
        $this->stopBehavior();
        $setting = $this->OptimisticLockBehavior->config();
        $version = $setting["version"];
        $message = $setting["error"]["message"];

        $sources = $this->OptimisticLocks->find('all')->toArray();

        $locked = $this->OptimisticLocks->optimisticLock($sources);

        foreach ($locked as $key => $entity) {
            $this->assertEquals( $sources[$key]->$version, $entity->$version, "lock findAll - 1");
            $this->assertTrue($this->OptimisticLocks->isOptimisticLocked($entity));
            $this->assertFalse($sources[$key]->dirty());
            $this->assertFalse($entity->dirty());
        }

        $datas = $this->OptimisticLocks->find('all')->toArray();
        foreach ($datas as $key => $entity) {
            $this->assertEquals( $sources[$key]->$version, $entity->$version, "lock findAll - 1");
            $this->assertTrue($this->OptimisticLocks->isOptimisticLocked($entity), "lock findAll - 2");
            $this->assertFalse($entity->dirty());
        }
    }

    public function testOffAutoWithFindMethod()
    {
        $this->stopBehavior();
        $setting = $this->OptimisticLockBehavior->config();
        $version = $setting["version"];
        $message = $setting["error"]["message"];

        $sources = $this->OptimisticLocks->find('all')->find('lock')->toArray();
        $datas = $this->OptimisticLocks->find('all')->toArray();
        foreach ($datas as $key => $entity) {
            $this->assertEquals( $sources[$key]->$version, $entity->$version, "lock findAll - 1");
            $this->assertTrue($this->OptimisticLocks->isOptimisticLocked($entity), "lock findAll - 2");
        }

        $sources = $this->OptimisticLocks->find('lock')->toArray();
        $datas = $this->OptimisticLocks->find('all')->toArray();
        foreach ($datas as $key => $entity) {
            $this->assertEquals( $sources[$key]->$version, $entity->$version, "lock findAll - 1");
            $this->assertTrue($this->OptimisticLocks->isOptimisticLocked($entity), "lock findAll - 2");
        }
    }

    public function testWithTimestampBehavior()
    {
        $this->stopBehavior();
        $this->OptimisticLocks->addBehavior('Timestamp');

        $sources = $this->OptimisticLocks->find('all')->toArray();
        $locked = $this->OptimisticLocks->find('lock')->toArray();

        foreach ($sources as $key => $entity) {
            $this->assertNotEmpty($locked[$key]->modified);
            $this->assertNotEquals($locked[$key]->modified, $entity->modified);
            $this->assertFalse($entity->dirty());
            $this->assertFalse($locked[$key]->dirty());
        }

        $sources = $this->OptimisticLocks->find('all')->toArray();
        foreach ($sources as $key => $entity) {
            $this->assertEquals($locked[$key]->modified, $entity->modified);
        }

        $this->OptimisticLocks->removeBehavior('Timestamp');
    }

    public function testWithTimestamp()
    {
        $this->stopBehavior();
        $this->OptimisticLockBehavior->config('timestamp', 'locked');

        $sources = $this->OptimisticLocks->find('all')->toArray();
        $locked = $this->OptimisticLocks->optimisticLock($this->OptimisticLocks->find('all')->toArray());

        foreach ($sources as $key => $entity) {
            $this->assertNotEmpty($locked[$key]->locked);
            $this->assertNotEquals($locked[$key]->locked, $entity->locked);
            $this->assertFalse($entity->dirty());
            $this->assertFalse($locked[$key]->dirty());
        }

        $sources = $this->OptimisticLocks->find('all')->toArray();
        foreach ($sources as $key => $entity) {
            $this->assertEquals($locked[$key]->modified, $entity->modified);
        }
    }

    public function testWithTimestampWithFindMethod()
    {
        $this->stopBehavior();
        $this->OptimisticLockBehavior->config('timestamp', 'locked');

        $sources = $this->OptimisticLocks->find('all')->toArray();
        $locked = $this->OptimisticLocks->find('lock')->toArray();

        foreach ($sources as $key => $entity) {
            $this->assertNotEmpty($locked[$key]->locked);
            $this->assertNotEquals($locked[$key]->locked, $entity->locked);
            $this->assertFalse($entity->dirty());
            $this->assertFalse($locked[$key]->dirty());
        }

        $sources = $this->OptimisticLocks->find('all')->toArray();
        foreach ($sources as $key => $entity) {
            $this->assertEquals($locked[$key]->modified, $entity->modified);
        }
    }

    public function testIfFindEmpty()
    {
        $datas = $this->OptimisticLocks
            ->find('all')
            ->where([
                'id' => 999999
            ])
            ->toArray();
         $this->assertEmpty($datas);
    }

    protected function _processSave($errorType , $is_multi = false)
    {
        if ($errorType === 'exception') {
            $this->_errorToException();
        }

        $setting = $this->OptimisticLockBehavior->config();
        $version = $setting["version"];
        $message = $setting["error"]["message"];

        // insert
        $entity = $this->OptimisticLocks->newEntity([
            "content" => "insert record",
        ]);
        $ret = $this->OptimisticLocks->save($entity);

        // asserting already records
        if ($is_multi) {
            $this->stopBehavior();
        }

        $datas = $this->OptimisticLocks
            ->find('all')
            ->toArray();
        $this->assertEmpty($datas[0]["version"],
            "no affect already exists record");
        $this->assertEquals("5302cfcf-ea10-43b3-9548-024b8c2d5341", $datas[1]["version"],
            "no affect already exists record");

        if ($is_multi) {
            $this->resumeBehavior();
        }

        // update
        $firstEntity = $this->OptimisticLocks
            ->find('all')
            ->where([
                'id' => 1
            ])
            ->first();
        $firstEntity->content = "update row1";
        // check : version is chenged null to uuid.
        $this->assertNotEmpty($firstEntity->$version, "version is not empty.");
        $ret = $this->OptimisticLocks->save($firstEntity);
        $this->assertNotEmpty($ret, "update row1");
        $this->assertEmpty($ret->errors(), "row1 validation empty");

        // find row
        $secondEntity = $this->OptimisticLocks
            ->find('all')
            ->where([
                'id' => 1
            ])
            ->first();
        $this->assertNotEmpty($secondEntity->$version, "version is not empty.");
        $this->assertNotEquals($firstEntity->$version, $secondEntity->$version, "version is not equals.");

        // save old row
        if ($errorType === 'message') {
            $firstEntity->set('content', 'can\'t not save');
            $ret = $this->OptimisticLocks->save($firstEntity);
            $this->assertFalse($ret, "be fail update row1");
            $this->assertEquals($message, $firstEntity->errors($version)[0], "row1 validation message was set");
        } else {
            try {
                $firstEntity->set('content', 'can\'t not save');
                $ret = $this->OptimisticLocks->save($firstEntity);
                $this->assertFalse(true, "must be occured exception");
            } catch (OptimisticLockException $e) {
                $this->assertEquals($message, $e->getMessage());
            }
        }

        // save new row
        $secondEntity->set("content", "update row2");
        $ret = $this->OptimisticLocks->save($secondEntity);
        $this->assertEmpty($ret->errors(), "row2 validation empty");
    }

    /**
     * testMultiSaveMessageWhenMultiLock
     *
     * @access public
     * @return void
     */
    protected function _processMultiSaveWhenMultiLock($errorType)
    {
        if ($errorType === 'exception') {
            $this->_errorToException();
        }

        $setting = $this->OptimisticLockBehavior->config();
        $version = $setting["version"];
        $message = $setting["error"]["message"];

        $this->stopBehavior();
        $sources = $this->OptimisticLocks
            ->find('all')
            ->toArray();
        $this->resumeBehavior();

        $datas = $this->OptimisticLocks
            ->find('all')
            ->toArray();

        foreach ($datas as $i => $val) {
            $this->assertNotEquals( $sources[$i]->$version, $datas[$i]->$version, "lock findAll - 1");
        }

        $firsts = $datas;
        $datas = $this->OptimisticLocks
            ->find('all')
            ->toArray();
        foreach ($datas as $i => $val) {
            $this->assertNotEquals( $firsts[$i]->$version, $datas[$i]->$version, "lock findAll - 2");
        }

        // test save fail
        if ($errorType === 'message') {
            $firstEntity = $firsts[0];
            $firstEntity->set("content", "update row1-1");
            $ret = $this->OptimisticLocks->save($firstEntity);
            $this->assertFalse($ret, "be fail update row1-1");
            $this->assertEquals($message, $firstEntity->errors($version)[0], "row1-1 validation message was set");

            $firstEntity = $firsts[1];
            $firstEntity->set("content", "update row1-2");
            $ret = $this->OptimisticLocks->save($firstEntity);
            $this->assertFalse($ret, "be fail update row1-2");
            $this->assertEquals($message, $firstEntity->errors($version)[0], "row1-2 validation message was set");
        } else {
            try {
                $firstEntity = $firsts[0];
                $firstEntity->set("content", "update row1-1");
                $ret = $this->OptimisticLocks->save($firstEntity);
            } catch (OptimisticLockException $e) {
                $this->assertEquals($message, $e->getMessage());
            }

            $first_data = $firsts[1];
            $first_data["OptimisticLock"]["content"] = "update row1-2";
            try {
                $firstEntity = $firsts[1];
                $firstEntity->set("content", "update row1-2");
                $ret = $this->OptimisticLocks->save($firstEntity);
            } catch (OptimisticLockException $e) {
                $this->assertEquals($message, $e->getMessage());
            }
        }

        // test save success
        $secondEntity = $datas[0];
        $secondEntity->set("content", "update row2-1");
        $ret = $this->OptimisticLocks->save($secondEntity);
        $this->assertEmpty($secondEntity->errors(), "row2-1 validation message was set");

        $secondEntity = $datas[1];
        $secondEntity->set("content", "update row2-2");
        $ret = $this->OptimisticLocks->save($secondEntity);
        $this->assertEmpty($secondEntity->errors(), "row2-2 validation message was set");
    }

    protected function _processMultiLockWhenMultiLock($errorType)
    {
        $this->stopBehavior();
        if ($errorType === 'exception') {
            $this->_errorToException();
        }
        $setting = $this->OptimisticLockBehavior->config();
        $version = $setting["version"];
        $message = $setting["error"]["message"];

        $sources = $this->OptimisticLocks
            ->find('all')
            ->toArray();


        $locks = $this->OptimisticLocks
            ->find('all')
            ->find('lock')
            ->where([
                'id in' => [1, 2]
            ])
            ->toArray();

        $afterSources = $this->OptimisticLocks
            ->find('all')
            ->toArray();

        for ($i = 0, $len = count($sources); $i < $len; $i++) {
            $method = $i < 2 ? "assertNotEquals" : "assertEquals";
            $this->{$method}(
                $sources[$i]->$version,
                $afterSources[$i]->$version,
                 "lock where in - index = $i");
        }
    }

    protected function resumeBehavior()
    {
        $this->OptimisticLockBehavior->config('auto', true);
    }

    protected function stopBehavior()
    {
        $this->OptimisticLockBehavior->config('auto', false);
    }

    protected function _errorToException()
    {
        $this->OptimisticLockBehavior->config('error', [
            "handler" => "exception",
            "message" => "exception occured",
        ]);
    }

    protected function _enableSingleConfig()
    {
        $this->OptimisticLockBehavior->config('single', true);
    }

}
