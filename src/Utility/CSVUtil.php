<?php
namespace Versatile\Utility;

/**
 * CSV reader class
 * @author konnok
 *
 */
class CSVReader {
    /**
     * delimiter
     *
     * @var string
     * @access public
     */
    public $delimiter = ",";
    /**
     * enclosure
     *
     * @var string
     * @access public
     */
    public $enclosure = '"';
    /**
     * path
     *
     * @var string
     * @access protected
     */
    protected $path = null;
    /**
     * encoding
     *
     * @var string
     * @access protected
     */
    protected $encoding = "SJIS-win";
    /**
     * to_encoding
     *
     * @var string
     * @access protected
     */
    protected $to_encoding = null;
    /**
     * fp
     *
     * @var file
     * @access protected
     */
    protected $fp = null;
    /**
     * eof
     *
     * @var bool
     * @access protected
     */
    protected $eof = false;
    /* protected __construct($path = null) {{{ */
    /**
     * constructor
     *
     * @param string $path
     * @access protected
     * @return void
     */
    function __construct($path = null) {
        $this->path = $path;
        $this->to_encoding = mb_internal_encoding();
    }
    // }}}

    /* public toEncoding($to_encoding) {{{ */
    /**
     * 変換先エンコーディングを設定します。
     *
     * @param string $to_encoding
     * @access public
     * @return $this
     */
    function toEncoding($to_encoding) {
        $this->to_encoding = $to_encoding;
        return $this;
    }
    // }}}

    /* public path($path) {{{ */
    /**
     * CSV パスを設定します。
     *
     * @param string $path
     * @access public
     * @return $this
     */
    function path($path) {
        $this->path = $path;
        $this->fp = null;
        $this->eof = false;
        return $this;
    }
    // }}}

    /* public encoding($encoding) {{{ */
    /**
     * 変換元エンコーディングを設定します。
     *
     * @param string $encoding
     * @access public
     * @return $this
     */
    function encoding($encoding) {
        $this->encoding = $encoding;
        return $this;
    }
    // }}}

    /* public readAll() {{{ */
    /**
     * ファイル内容全てを読み込んだ結果を返却します。
     *
     * @access public
     * @return array
     */
    function readAll() {
        $lines = array();
        while (($line = $this->read()) !== false) {
            $lines[] = $line;
        }
        return $lines;
    }
    // }}}

    /* public read() {{{ */
    /**
     * 一行読み込んだ結果を返却します。
     *
     * @access public
     * @return array|false
     */
    function read() {
        if (!isset($this->fp)) {
            $src = preg_replace('/\r\n|\r|\n/', "\n", file_get_contents($this->path));
            $src = mb_convert_encoding($src, $this->to_encoding, mb_detect_encoding($src));
            $fp = fopen('php://temp/maxmemory:'. (5*1024*1024), 'r+');
            fwrite($fp, $src);
            rewind($fp);
            $this->fp = $fp;
        }
        $fp = $this->fp;
        $line = $this->fgetcsv_reg($fp, null, $this->delimiter, $this->enclosure);
        return $line;
    }
    // }}}

    /* public isEof() {{{ */
    /**
     * EOF かどうか判定した結果を返却します。
     *
     * @access public
     * @return void
     */
    function isEof() {
        if (isset($this->fp)) {
            return feof($this->fp);
        }
        return false;
    }
    // }}}

    /* public fgetcsv_reg(&$handle, $length = null, $d = ',', $e = '"') {{{ */
    /**
     * csv のパースを実施します。
     *
     * @param resource $handle
     * @param integer $length
     * @param string $d
     * @param string $e
     * @access public
     * @return array
     */
    function fgetcsv_reg(&$handle, $length = null, $d = ',', $e = '"') {
        $eof = false;
        $d = preg_quote($d);
        $e = preg_quote($e);
        $_line = "";
        while ($eof != true) {
            $_line .= (empty($length) ? fgets($handle) : fgets($handle, $length));
            $itemcnt = preg_match_all('/'.$e.'/', $_line, $dummy);
            if ($itemcnt % 2 == 0) $eof = true;
        }
        $_csv_line = preg_replace('/(?:\\r\\n|[\\r\\n])?$/', $d, trim($_line));
        $_csv_pattern = '/('.$e.'[^'.$e.']*(?:'.$e.$e.'[^'.$e.']*)*'.$e.'|[^'.$d.']*)'.$d.'/';
        preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);
        $_csv_data = $_csv_matches[1];
        for($_csv_i=0;$_csv_i<count($_csv_data);$_csv_i++){
            $_csv_data[$_csv_i]=preg_replace('/^'.$e.'(.*)'.$e.'$/s','$1',$_csv_data[$_csv_i]);
            $_csv_data[$_csv_i]=str_replace($e.$e, $e, $_csv_data[$_csv_i]);
        }
        return empty($_line) ? false : $_csv_data;
    }
    // }}}
}

class CSVWriterBase {
    public $path;
    /**
     * delimiter
     *
     * @var string
     * @access public
     */
    public $delimiter = ",";
    /**
     * enclosure
     *
     * @var string
     * @access public
     */
    public $enclosure = '"';
    /**
     * nullString
     *
     * @var string
     * @access public
     */
    public $nullString = null;

    /**
     * レコード保持用バッファ
     *
     * @var array
     * @access protected
     */
    protected $line = null;
    /**
     * CSV 一時書き込みリソース
     *
     * @var resource
     * @access protected
     */
    protected $buffer = null;
    /**
     * encoding
     *
     * @var string
     * @access protected
     */
    protected $encoding = "SJIS-win";
    /**
     * from_encoding
     *
     * @var string
     * @access protected
     */
    protected $from_encoding = null;
    /* protected __construct() {{{ */
    /**
     * constructor
     *
     * @access protected
     * @return void
     */
    function __construct() {
        $this->from_encoding = mb_internal_encoding();
        $this->clear();
    }
    // }}}
    /* public fromEncoding($from_encoding) {{{ */
    /**
     * fromEncoding
     *
     * @param string $from_encoding
     * @access public
     * @return $this
     */
    function fromEncoding($from_encoding) {
        $this->from_encoding = $from_encoding;
        return $this;
    }
    // }}}
    /* public encoding($encoding) {{{ */
    /**
     * encoding
     *
     * @param string $encoding
     * @access public
     * @return $this
     */
    function encoding($encoding) {
        $this->encoding = $encoding;
        return $this;
    }
    // }}}
    /* public clear() {{{ */
    /**
     * clear
     *
     * @access public
     * @return $this
     */
    function clear() {
        $this->records = array();
        $this->line = array();
        return $this->openFile();
    }
    // }}}

    /* public add($item) {{{ */
    /**
     * add
     *
     * @param mixed $item
     * @access public
     * @return $this
     */
    function add($item) {
        $this->line[] = $item;
        return $this;
    }
    // }}}

    function addEmpty($count = 1) {
        for ($i = 0; $i < $count; $i++) {
            $this->add("");
        }
        return $this;
    }

    /* public next() {{{ */
    /**
     * next
     *
     * @access public
     * @return $this
     */
    function next() {
        $this->fputcsv_reg($this->buffer, $this->line, $this->delimiter, $this->enclosure, $this->nullString);
        $this->line = array();
        return $this;
    }
    // }}}

    /* public addLine($items) {{{ */
    /**
     * CSV 行データをセットします。
     *
     * @param array|string $items
     * @access public
     * @return $this
     */
    function addLine($items) {
        if (!is_array($items)) {
            $items = func_get_args();
        }
        $this->line = array_merge($this->line, $items);
        return $this->next();
    }
    // }}}

    /* public grid($grid) {{{ */
    /**
     * csv データを一括セットします。
     *
     * @param mixed $grid
     * @access public
     * @return $this
     */
    function grid($grid) {
        foreach ($grid as $line) {
            $this->addLine($line);
        }
        return $this;
    }
    // }}}

    /**
     * CSVから1行読み込みます。
     *
     * @param resource $fh
     * @param array $fields
     * @param string $delimiter
     * @param string $enclosure
     * @param string $null_string
     * @access public
     * @return void
     */
    function fputcsv_reg($fh, array $fields, $delimiter = ',', $enclosure = '"', $null_string = false) {
        $delimiter_esc = preg_quote($delimiter, '/');
        $enclosure_esc = preg_quote($enclosure, '/');

        $output = array();
        foreach ($fields as $field) {
            if ($field === null && $null_string) {
                $output[] = $null_string;
                continue;
            }

            $output[] = preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field) ? (
                $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure
            ) : $field;
        }

        fwrite($fh, implode($delimiter, $output) . "\r\n");
    }

    function isMSIE() {
        $ua = $_SERVER['HTTP_USER_AGENT'];
        return preg_match('/msie/i', $ua) > 0;
    }

    function normalizeFilename($filename) {
        return str_replace(array(
            ":", ";", "/", "\\", "|", ",", "*", "?", "\"", "<", ">", "~"
            ), array(
            "：", "；", "／", "￥", "｜", "、", "＊", "？", "”", "＜", "＞", "〜"
            ), $filename);
    }

    function openFile() {
        throw new BadFunctionCallException("implements method is required");
    }

    function printHttpHeader($filename) {
        $filename = $this->normalizeFilename($filename);
        if ($this->isMSIE()) {
            $filename = mb_convert_encoding($filename, "SJIS-win", $this->from_encoding);
        }
        header("Content-type:application/vnd.ms-excel");
        header("Content-disposition:attachment; filename=\"{$filename}\"");
    }
}

/**
 * CSV writer class
 * @author konnok
 *
 */
class CSVWriter extends CSVWriterBase {
    /**
     * fd をオープンします。
     *
     * @access public
     * @return void
     */
    function openFile() {
        $this->buffer = fopen('php://temp/maxmemory:'. (5*1024*1024), 'r+');
        return $this;
    }
    /* public render() {{{ */
    /**
     * CSVを返却します。
     *
     * @access public
     * @return $this
     */
    function render() {
        rewind($this->buffer);
        $s = stream_get_contents($this->buffer);
        if ($this->encoding) {
            $s = mb_convert_encoding($s, $this->encoding, $this->from_encoding);
        }
        return $s;
    }
    // }}}

    /* public response($filename, $contents = null) {{{ */
    /**
     * CSV レスポンスを返却します。
     *
     * @param string $filename
     * @param string $contents
     * @param boolean $exit
     * @access public
     * @return void
     */
    function response($filename, $contents = null, $exit = false) {
        if (!isset($contents)) {
            $contents = $this->render();
        }
        $this->printHttpHeader($filename);
        echo $contents;
        if ($exit) {
            exit(0);
        }
    }
    // }}}

    function close() {
        // do nothing;
    }
}

class CSVFileWriter extends CSVWriter {
    var $path = null;

    function __construct($path) {
        $this->path = $path;
        parent::__construct();
    }

    function openFile() {
        $this->buffer = fopen($this->path, 'w+');
        return $this;
    }

    function next() {
        mb_convert_variables($this->encoding, $this->from_encoding, $this->line);
        return parent::next();
    }

    function close() {
        $ret = fclose($this->buffer);
        if ($ret) {
            $this->buffer = null;
        }
        return $ret;
    }

    function removeFile() {
        return unlink($this->path);
    }

    function responseFile($output_filename = null) {
        is_null($output_filename) && $output_filename = basename($this->path);
        if ($this->buffer) {
            $this->close();
        }
        $this->printHttpHeader($output_filename);
        header('Content-Length: ' . filesize($this->path));
        readfile($this->path);
        sleep(1);
    }

}

/**
 * Csv reader/writer factory
 * @author konnok
 *
 */
class CSVUtil {
    /* public read($path = null) {{{ */
    /**
     * CSV Reader オブジェクトを返却します。
     *
     * @param string $path
     * @static
     * @access public
     * @return CSVReader
     */
    static function getReader($path = null) {
        return new CSVReader($path);
    }
    // }}}

    /* public getCreator() {{{ */
    /**
     * CSVWriter オブジェクトを返却します。
     *
     * @static
     * @access public
     * @return CSVWriter
     */
    static function getWriter() {
        return new CSVWriter();
    }
    // }}}

    static function getFileWriter($path) {
        return new CSVFileWriter($path);
    }
}
