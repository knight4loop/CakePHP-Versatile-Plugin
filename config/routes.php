<?php
use Cake\Routing\Router;

Router::plugin(
    'Versatile',
    ['path' => '/versatile'],
    function ($routes) {
        $routes->fallbacks('DashedRoute');
    }
);
