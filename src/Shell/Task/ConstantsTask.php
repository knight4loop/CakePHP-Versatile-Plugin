<?php
/**
 * ConstantsTask
 *
 * @package
 * @copyright
 * @author k.naito <k.naito@coosy.co.jp>
 */
namespace Versatile\Shell\Task;

use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Utility\Inflector;
use Versatile\Datafile\Converter;
use Versatile\Datafile\Datafile;
use Versatile\Console\VersatileShell;

class ConstantsTask extends VersatileShell
{
    public $tasks = ['Versatile.File'];

    /**
     * Gets the option parser instance and configures it.
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->description(
            "ファイルからを読み取って定数 Class を作成します。\n" .
            "読み取るファイルはコマンドラインオプションで指定してください。"
        )->addOption('path', [
            'help' => "指定したファイルもしくはディレクトリが使用されます。",
            'default' => Configure::read('Versatile.Constants.path'),
            'short' => 'p'
        ])->addOption('labels-path', [
            'help' => "指定したファイルが使用されます。",
            'default' => Configure::read('Versatile.Labels.path'),
            'short' => 'l'
        ])->addOption('file', [
            'help' => "指定したファイルもしくはディレクトリが使用されます。",
            'default' => Configure::read('Versatile.Constants.file'),
            'short' => 'o'
        ])->addOption('force', [
            'short' => 'f',
            'help' => '取り込みの際の確認メッセージを表示しません。',
            'boolean' => true,
        ]);

        return $parser;
    }

    public function startup ()
    {
        parent::startup();
        $this->Datafile = new Datafile();
    }

    public function main()
    {
        $codes = array_merge(
            $this->_makeLabelConstants(),
            $this->_makeConstants()
        );

        $result = "<?php\n\n" . implode("\n\n", $codes);
        $fpath = $this->param('file');
        file_put_contents($fpath, $result);
        $this->out("generate code => $fpath");
    }

    protected function _makeLabelConstants()
    {
        $path = realpath($this->param('labels-path'));

        $files = $this->Datafile->find($path);

        foreach ($files as $idx => $file) {
            $engineName = $this->Datafile->getEngineName($file);
            $Engine = $this->Datafile->getEngine($engineName);
            if (!$Engine) {
                continue;
            }
            $sheets = $Engine
                ->clear()
                ->path($path)
                ->sheets();
            foreach ($sheets as $sheet) {
                $sheetName = pathinfo($sheet)['filename'];
                $sheetName = Inflector::tableize($sheetName);
                if ($engineName === 'excel' && $sheetName !== 'labels') {
                    continue;
                }
                $data = $Engine->read($sheet);
                $data = Converter::convert($data, 'FileLabelToArray');
                foreach ($data as $idx => $line) {
                    $hash[$line['category']][] =
                        sprintf( "/** @var %s */  const %s = %s;",
                        $line['label'],
                        $line['constant'],
                        is_numeric($line['value']) ? $line['value'] : "\"{$line['value']}\""
                    );
                }
            }
        }

        $codes = [];
        foreach ($hash as $category => $lines) {
            $codes[] = $this->_buildClassCode($category, $lines);
        }
        return $codes;
    }

    protected function _makeConstants ()
    {
        $path = realpath($this->param('path'));

        $files = $this->Datafile->find($path);

        $hash = [];
        foreach ($files as $idx => $file) {
            $Engine = $this->Datafile->getEngine($file);
            if (!$Engine) {
                continue;
            }
            $sheets = $Engine->sheets();
            foreach ($sheets as $sheet) {
                $sheetName = pathinfo($sheet)['filename'];
                $sheetName = Inflector::classify($sheetName);
                $data = $Engine->read($sheet);
                $data = Converter::convert($data, 'ArrayToHash');
                foreach ($data as $idx => $line) {
                    $name = key($line);
                    $value = current($line);
                    if ($name == "" || $value == "") {
                        continue;
                    }
                    $value = preg_replace("!\r\n|\r|\n!", "\\n", $value);
                    $hash[$sheetName][] = sprintf("/** @var %s */\n\tconst %s = %s;", $value, $name, is_numeric($value) ? $value : "\"$value\"");
                }
            }
        }
        $codes = [];
        foreach ($hash as $category => $lines) {
            $codes[] = $this->_buildInterfaceCode($category, $lines);
        }
        return $codes;
    }

    protected function _buildInterfaceCode($category, $lines) {
        if (empty($lines)) {
            return "";
        }
        $klass_name = "I" . Inflector::camelize($category);// . "Constant";
        return sprintf("interface %s\n{\n\t%s\n}", $klass_name, implode("\n\t", $lines));
    }

    protected function _buildClassCode($category, $lines) {
        if (empty($lines)) {
            return "";
        }
        $klass_name = "C" . Inflector::camelize($category);// . "Constant";
        return sprintf("class %s extends \Versatile\Core\Constant \n{\n\t%s\n}", $klass_name, implode("\n\t", $lines));
    }
}
