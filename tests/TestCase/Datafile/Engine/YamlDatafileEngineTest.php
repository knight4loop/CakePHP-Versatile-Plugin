<?php
namespace Versatile\Test\TestCase\Datafile\Engine;

use Cake\Core\Plugin;
use Cake\TestSuite\TestCase;
use Versatile\Datafile\Engine\YamlDatafileEngine;

class YamlDatafileEngineTest extends TestCase
{
    protected $_datasDir = 'samples' . DS . 'tables';

    protected $_path = null;

    protected $_labelsArray = [
        [
            'category' => 'gender',
            'label' => '男性',
            'value' => 1,
            'constant' => 'MALE'
        ],
        [
            'category' => 'gender',
            'label' => '女性',
            'value' => 2,
            'constant' => 'FEMALE'
        ],
        [
            'category' => 'is_deleted',
            'label' => '-',
            'value' => 0,
            'constant' => 'EXIST'
        ],
        [
            'category' => 'is_deleted',
            'label' => '削除済',
            'value' => 1,
            'constant' => 'DELETED'
        ]
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->_path = Plugin::path('Versatile') . $this->_datasDir . DS;
    }


    public function testRead()
    {
        $YamlDatafile = new YamlDatafileEngine();
        $labels = $YamlDatafile->path($this->_path . 'labels.yaml')->read();
        $this->assertEquals($labels, $this->_labelsArray);
    }

}
