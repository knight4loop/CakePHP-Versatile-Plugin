<?php
namespace Versatile\Test\TestCase\Datafile;

use Cake\Core\Plugin;
use Cake\TestSuite\TestCase;
use Cake\Utility\Text;
use Versatile\Datafile\Datafile;

class DatafileTest extends TestCase
{
    protected $_datasDir = 'samples' . DS;

    protected $_maps = [
        'Versatile\\Datafile\\Engine\\YamlDatafileEngine' => [
            'yaml',
            '.yaml',
            '{:dir}labels.yaml'
        ],
        'Versatile\\Datafile\\Engine\\CsvDatafileEngine' => [
            'csv',
            '.csv',
            '{:dir}labels.csv'
        ],
        'Versatile\\Datafile\\Engine\\ExcelDatafileEngine' => [
            'excel',
            '.xls',
            '.xlsx',
            '{:dir}initial_data.xlsx'
        ],
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Datafile = new Datafile;

        $dir = Plugin::path('Versatile') . $this->_datasDir . 'tables' . DS;
        foreach ($this->_maps as $className => &$paths) {
            foreach ($paths as $idx => &$path) {
                $path = Text::insert($path, compact('dir'), [
                    'before' => '{:',
                    'after' => '}',
                ]);
            }
            unset($path);
        }
        unset($paths);
    }

    public function testGetEngineName()
    {
        foreach ($this->_maps as $className => $paths) {
            $engineName = array_shift($paths);
            foreach ($paths as $path) {
                $_engineName = $this->Datafile->getEngineName($path);
                $this->assertEquals($_engineName, $engineName);
            }
        }
        $this->assertEquals($this->Datafile->getEngineName(null), null);
        $this->assertEquals($this->Datafile->getEngineName('txt'), null);
        $this->assertEquals($this->Datafile->getEngineName('.txt'), null);
    }

    public function testGetEngine()
    {
        foreach ($this->_maps as $className => $paths) {
            foreach ($paths as $path) {
                $obj = $this->Datafile->getEngine($path);
                $this->assertEquals(get_class($obj), $className);
            }
        }
        $this->assertEquals($this->Datafile->getEngine(null), null);
        $this->assertEquals($this->Datafile->getEngine('txt'), null);
        $this->assertEquals($this->Datafile->getEngine('.txt'), null);
    }

    public function testGetExtensions()
    {
        $extsAll = $this->Datafile->getExtensions();

        foreach ($extsAll as $name => $exts) {
            $_exts = $this->Datafile->getExtensions($name);
            $this->assertEquals($_exts[$name], $exts);
        }

        $keys = array_keys($extsAll);

        $this->assertEquals(call_user_func_array([$this->Datafile, 'getExtensions'], $keys), $extsAll);
        $this->assertEquals($this->Datafile->getExtensions($keys), $extsAll);
        $this->assertEquals($this->Datafile->getExtensions('txt'), []);
    }

    public function testFind()
    {
        $path = Plugin::path('Versatile') . $this->_datasDir . 'tables' . DS;

        $result = $this->Datafile->find($path, [
            'name' => 'labels'
        ]);
        foreach ($result as $filePath) {
            $fileinfo = pathinfo($filePath);
            $this->assertEquals(strtolower($fileinfo['filename']), 'labels');
        }

        $tmpExts = $this->Datafile->getExtensions('yaml', 'excel');
        $exts = [];
        foreach ($tmpExts as $_exts) {
            $exts = array_merge($exts, $_exts);
        }
        $result = $this->Datafile->find($path, [
            'engineName' => 'yaml', 'excel'
        ]);
        foreach ($result as $filePath) {
            $fileinfo = pathinfo($filePath);
            $this->assertContains('.' . strtolower($fileinfo['extension']), $exts);
        }

        $result = $this->Datafile->find($path, [
            'name' => 'labels',
            'engineName' => 'csv',
        ]);
        foreach ($result as $filePath) {
            $fileinfo = pathinfo($filePath);
            $this->assertEquals(strtolower($fileinfo['filename']), 'labels');
            $this->assertEquals(strtolower($fileinfo['extension']), 'csv');
        }
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Datafile);

        parent::tearDown();
    }
}
