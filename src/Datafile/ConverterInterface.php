<?php
namespace Versatile\Datafile;

interface ConverterInterface
{
    abstract public static function exec($data);
}
